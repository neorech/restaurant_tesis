-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generación: 22-11-2021 a las 03:03:33
-- Versión del servidor: 5.0.51
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de datos: `restaurante`
-- 

-- --------------------------------------------------------

--   
-- Estructura de tabla para la tabla `almacen`
-- 

CREATE TABLE `almacen` (
  `idAlmacen` mediumint(3) NOT NULL auto_increment,
  `aProducto` varchar(100) NOT NULL,
  `aCantidad` varchar(5) NOT NULL,
  `aFecha` datetime NOT NULL,
  `aUnidadMedida` varchar(20) NOT NULL,
  PRIMARY KEY  (`idAlmacen`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

-- 
-- Volcar la base de datos para la tabla `almacen`
-- 

INSERT INTO `almacen` VALUES (1, 'Pescado', '0', '0000-00-00 00:00:00', 'kilos');
INSERT INTO `almacen` VALUES (2, 'Algas', '0', '0000-00-00 00:00:00', 'hojas');
INSERT INTO `almacen` VALUES (3, 'Pollo', '28', '0000-00-00 00:00:00', 'kilos');
INSERT INTO `almacen` VALUES (4, 'Langostinos', '28.5', '0000-00-00 00:00:00', 'kilos');
INSERT INTO `almacen` VALUES (5, 'Coca cola', '9', '0000-00-00 00:00:00', 'Botella');
INSERT INTO `almacen` VALUES (6, 'Cuzquena Golden', '101', '0000-00-00 00:00:00', 'Botella');
INSERT INTO `almacen` VALUES (7, 'Cuzquena Negra', '42', '0000-00-00 00:00:00', 'Botella');
INSERT INTO `almacen` VALUES (8, 'Cuzquena Trigo', '25', '0000-00-00 00:00:00', 'Botella');
INSERT INTO `almacen` VALUES (9, 'Lomo Fino', '0', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (10, 'Corazon de Res', '7', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (11, 'Pollo para Taco', '9', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (12, 'Pollo para Escabeche', '6', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (13, 'Pato', '3', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (14, 'Puerco', '11', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (15, 'Cordero', '5', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (16, 'Canilla Cordero', '2', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (17, 'Cuy', '5', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (18, 'Trucha Chicharron', '17', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (19, 'Trucha Ceviche', '1', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (20, 'Trucha Tiradito', '1', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (21, 'Trucha Causa', '0', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (22, 'Trucha Palta', '0', '0000-00-00 00:00:00', 'Porcion');
INSERT INTO `almacen` VALUES (23, 'Trucha Chupin', '4', '0000-00-00 00:00:00', 'Porcion');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `almacencontrol`
-- 

CREATE TABLE `almacencontrol` (
  `idAlmacenControl` mediumint(5) NOT NULL auto_increment,
  `acDetalle` varchar(500) NOT NULL,
  `acFecha` datetime NOT NULL,
  `pId` varchar(5) NOT NULL,
  `acMovimiento` varchar(100) NOT NULL,
  PRIMARY KEY  (`idAlmacenControl`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

-- 
-- Volcar la base de datos para la tabla `almacencontrol`
-- 

INSERT INTO `almacencontrol` VALUES (52, 'COMPRA DE CERVEZA MONTO DE 500 NUEVOS SOLES - FRANCISCA', '2015-05-04 20:37:55', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (53, '6 CHELAS INVITACION DE SOCIO.', '2015-05-04 20:38:42', '3', 'Egreso');
INSERT INTO `almacencontrol` VALUES (54, '', '2015-05-07 17:16:05', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (55, '', '2015-05-15 09:34:56', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (56, '', '2015-05-15 09:36:08', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (57, '', '2015-05-15 09:41:34', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (51, 'BOLETA POR COMPRA DE CARNES COSTO 300 SOLES.', '2015-05-04 20:36:51', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (58, '', '2017-02-08 12:38:00', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (59, '', '2017-02-08 12:58:08', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (60, '', '2017-02-08 13:02:09', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (61, '', '2017-02-09 12:27:58', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (62, '', '2017-02-09 13:23:32', '3', 'Egreso');
INSERT INTO `almacencontrol` VALUES (63, '', '2017-02-11 11:24:21', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (64, '', '2017-02-11 11:27:53', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (65, '', '2017-02-11 11:29:02', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (66, '', '2017-02-11 11:30:00', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (67, '', '2017-02-11 11:30:23', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (68, '', '2017-02-11 16:10:00', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (69, '', '2017-02-12 16:45:07', '3', 'Egreso');
INSERT INTO `almacencontrol` VALUES (70, '', '2017-02-14 12:52:40', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (71, '', '2017-02-14 12:54:55', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (72, '', '2017-02-14 15:12:00', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (73, '', '2017-02-15 12:20:53', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (74, '', '2017-02-15 14:15:05', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (75, 'chancho', '2017-02-15 14:22:39', '3', '');
INSERT INTO `almacencontrol` VALUES (76, '', '2017-02-16 13:07:26', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (77, '', '2017-02-16 13:16:07', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (78, '', '2017-02-16 13:16:28', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (79, '', '2017-02-17 12:14:56', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (80, '', '2017-02-17 12:20:36', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (81, '', '2017-02-18 10:32:42', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (82, '', '2017-02-18 10:33:02', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (83, '', '2017-02-18 10:33:30', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (84, '', '2017-02-18 10:34:03', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (85, '', '2017-02-18 10:34:41', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (86, '', '2017-02-19 11:16:25', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (87, 'Boleta no 100', '2020-08-12 17:50:52', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (88, '', '2021-09-25 17:00:23', '3', 'Ingreso');
INSERT INTO `almacencontrol` VALUES (89, 'Se cayo una botella de gaseosa', '2021-09-25 17:04:58', '3', 'Egreso');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `almacencontroldetalle`
-- 

CREATE TABLE `almacencontroldetalle` (
  `idAlmacenControlDetalle` mediumint(5) NOT NULL auto_increment,
  `idAlmacenControl` varchar(5) NOT NULL,
  `idAlmacen` varchar(5) NOT NULL,
  `acdCantidad` varchar(5) NOT NULL,
  PRIMARY KEY  (`idAlmacenControlDetalle`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=471 ;

-- 
-- Volcar la base de datos para la tabla `almacencontroldetalle`
-- 

INSERT INTO `almacencontroldetalle` VALUES (1, '3', '4', '20');
INSERT INTO `almacencontroldetalle` VALUES (2, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (3, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (4, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (5, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (6, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (7, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (8, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (9, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (10, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (11, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (12, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (13, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (14, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (15, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (16, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (17, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (18, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (19, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (20, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (21, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (22, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (23, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (24, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (25, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (26, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (27, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (28, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (29, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (30, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (31, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (32, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (33, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (34, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (35, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (36, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (37, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (38, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (39, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (40, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (41, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (42, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (43, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (44, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (45, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (46, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (47, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (48, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (49, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (50, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (51, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (52, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (53, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (54, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (55, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (56, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (57, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (58, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (59, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (60, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (61, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (62, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (63, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (64, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (65, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (66, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (67, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (68, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (69, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (70, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (71, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (72, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (73, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (74, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (75, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (76, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (77, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (78, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (79, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (80, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (81, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (82, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (83, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (84, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (85, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (86, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (87, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (88, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (89, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (90, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (91, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (92, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (93, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (94, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (95, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (96, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (97, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (98, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (99, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (100, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (101, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (102, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (103, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (104, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (105, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (106, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (107, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (108, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (109, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (110, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (111, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (112, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (113, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (114, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (115, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (116, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (117, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (118, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (119, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (120, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (121, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (122, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (123, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (124, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (125, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (126, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (127, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (128, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (129, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (130, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (131, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (132, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (133, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (134, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (135, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (136, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (137, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (138, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (139, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (140, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (141, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (142, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (143, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (144, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (145, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (146, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (147, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (148, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (149, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (150, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (151, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (152, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (153, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (154, '1', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (155, '111', '1', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (156, '111', '2', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (157, '111', '3', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (158, '111', '4', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (159, '111', '5', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (160, '111', '6', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (161, '111', '7', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (162, '111', '8', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (163, '111', '9', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (164, '111', '10', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (165, '111', '11', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (166, '111', '12', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (167, '111', '13', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (168, '111', '14', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (169, '111', '15', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (170, '111', '16', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (171, '111', '17', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (172, '111', '18', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (173, '111', '19', 'aCant');
INSERT INTO `almacencontroldetalle` VALUES (174, '111', '1', '');
INSERT INTO `almacencontroldetalle` VALUES (175, '111', '2', '');
INSERT INTO `almacencontroldetalle` VALUES (176, '111', '3', '');
INSERT INTO `almacencontroldetalle` VALUES (177, '111', '4', '');
INSERT INTO `almacencontroldetalle` VALUES (178, '111', '5', '');
INSERT INTO `almacencontroldetalle` VALUES (179, '111', '6', '');
INSERT INTO `almacencontroldetalle` VALUES (180, '111', '7', '');
INSERT INTO `almacencontroldetalle` VALUES (181, '111', '8', '');
INSERT INTO `almacencontroldetalle` VALUES (182, '111', '9', '');
INSERT INTO `almacencontroldetalle` VALUES (183, '111', '10', '');
INSERT INTO `almacencontroldetalle` VALUES (184, '111', '11', '');
INSERT INTO `almacencontroldetalle` VALUES (185, '111', '12', '');
INSERT INTO `almacencontroldetalle` VALUES (186, '111', '13', '');
INSERT INTO `almacencontroldetalle` VALUES (187, '111', '14', '');
INSERT INTO `almacencontroldetalle` VALUES (188, '111', '15', '');
INSERT INTO `almacencontroldetalle` VALUES (189, '111', '16', '');
INSERT INTO `almacencontroldetalle` VALUES (190, '111', '17', '');
INSERT INTO `almacencontroldetalle` VALUES (191, '111', '18', '');
INSERT INTO `almacencontroldetalle` VALUES (192, '111', '19', '');
INSERT INTO `almacencontroldetalle` VALUES (193, '111', '1', '33');
INSERT INTO `almacencontroldetalle` VALUES (194, '111', '2', '333');
INSERT INTO `almacencontroldetalle` VALUES (195, '111', '3', '3333');
INSERT INTO `almacencontroldetalle` VALUES (196, '111', '4', '0');
INSERT INTO `almacencontroldetalle` VALUES (197, '111', '5', '3');
INSERT INTO `almacencontroldetalle` VALUES (198, '111', '6', '3');
INSERT INTO `almacencontroldetalle` VALUES (199, '111', '7', '3');
INSERT INTO `almacencontroldetalle` VALUES (200, '111', '8', '3');
INSERT INTO `almacencontroldetalle` VALUES (201, '111', '9', '3');
INSERT INTO `almacencontroldetalle` VALUES (202, '111', '10', '3');
INSERT INTO `almacencontroldetalle` VALUES (203, '111', '11', '3');
INSERT INTO `almacencontroldetalle` VALUES (204, '111', '12', '3');
INSERT INTO `almacencontroldetalle` VALUES (205, '111', '13', '333');
INSERT INTO `almacencontroldetalle` VALUES (206, '111', '14', '0');
INSERT INTO `almacencontroldetalle` VALUES (207, '111', '15', '3333');
INSERT INTO `almacencontroldetalle` VALUES (208, '111', '16', '33');
INSERT INTO `almacencontroldetalle` VALUES (209, '111', '17', '3333');
INSERT INTO `almacencontroldetalle` VALUES (210, '111', '18', '33333');
INSERT INTO `almacencontroldetalle` VALUES (211, '111', '19', '33333');
INSERT INTO `almacencontroldetalle` VALUES (212, '111', '1', '33');
INSERT INTO `almacencontroldetalle` VALUES (213, '111', '2', '333');
INSERT INTO `almacencontroldetalle` VALUES (214, '111', '3', '3333');
INSERT INTO `almacencontroldetalle` VALUES (215, '111', '4', '0');
INSERT INTO `almacencontroldetalle` VALUES (216, '111', '5', '3');
INSERT INTO `almacencontroldetalle` VALUES (217, '111', '6', '3');
INSERT INTO `almacencontroldetalle` VALUES (218, '111', '7', '3');
INSERT INTO `almacencontroldetalle` VALUES (219, '111', '8', '3');
INSERT INTO `almacencontroldetalle` VALUES (220, '111', '9', '3');
INSERT INTO `almacencontroldetalle` VALUES (221, '111', '10', '3');
INSERT INTO `almacencontroldetalle` VALUES (222, '111', '11', '3');
INSERT INTO `almacencontroldetalle` VALUES (223, '111', '12', '3');
INSERT INTO `almacencontroldetalle` VALUES (224, '111', '13', '333');
INSERT INTO `almacencontroldetalle` VALUES (225, '111', '14', '0');
INSERT INTO `almacencontroldetalle` VALUES (226, '111', '15', '3333');
INSERT INTO `almacencontroldetalle` VALUES (227, '111', '16', '33');
INSERT INTO `almacencontroldetalle` VALUES (228, '111', '17', '3333');
INSERT INTO `almacencontroldetalle` VALUES (229, '111', '18', '33333');
INSERT INTO `almacencontroldetalle` VALUES (230, '111', '19', '33333');
INSERT INTO `almacencontroldetalle` VALUES (231, '26', '1', '2030');
INSERT INTO `almacencontroldetalle` VALUES (232, '26', '2', '30');
INSERT INTO `almacencontroldetalle` VALUES (233, '26', '3', '40');
INSERT INTO `almacencontroldetalle` VALUES (234, '26', '4', '50');
INSERT INTO `almacencontroldetalle` VALUES (235, '26', '5', '60');
INSERT INTO `almacencontroldetalle` VALUES (236, '26', '6', '70');
INSERT INTO `almacencontroldetalle` VALUES (237, '26', '7', '80');
INSERT INTO `almacencontroldetalle` VALUES (238, '26', '8', '90');
INSERT INTO `almacencontroldetalle` VALUES (239, '26', '9', '3');
INSERT INTO `almacencontroldetalle` VALUES (240, '26', '10', '3');
INSERT INTO `almacencontroldetalle` VALUES (241, '26', '11', '3');
INSERT INTO `almacencontroldetalle` VALUES (242, '26', '12', '3');
INSERT INTO `almacencontroldetalle` VALUES (243, '26', '13', '333');
INSERT INTO `almacencontroldetalle` VALUES (244, '26', '14', '0');
INSERT INTO `almacencontroldetalle` VALUES (245, '26', '15', '3333');
INSERT INTO `almacencontroldetalle` VALUES (246, '26', '16', '33');
INSERT INTO `almacencontroldetalle` VALUES (247, '26', '17', '3333');
INSERT INTO `almacencontroldetalle` VALUES (248, '26', '18', '33333');
INSERT INTO `almacencontroldetalle` VALUES (249, '26', '19', '33333');
INSERT INTO `almacencontroldetalle` VALUES (250, '27', '1', '99');
INSERT INTO `almacencontroldetalle` VALUES (251, '27', '2', '99');
INSERT INTO `almacencontroldetalle` VALUES (252, '27', '3', '99');
INSERT INTO `almacencontroldetalle` VALUES (253, '27', '4', '99');
INSERT INTO `almacencontroldetalle` VALUES (254, '27', '5', '9');
INSERT INTO `almacencontroldetalle` VALUES (255, '27', '6', '9');
INSERT INTO `almacencontroldetalle` VALUES (256, '27', '7', '9');
INSERT INTO `almacencontroldetalle` VALUES (257, '27', '8', '99');
INSERT INTO `almacencontroldetalle` VALUES (258, '27', '9', '99');
INSERT INTO `almacencontroldetalle` VALUES (259, '27', '10', '999');
INSERT INTO `almacencontroldetalle` VALUES (260, '27', '11', '99');
INSERT INTO `almacencontroldetalle` VALUES (261, '27', '12', '9');
INSERT INTO `almacencontroldetalle` VALUES (262, '27', '13', '9');
INSERT INTO `almacencontroldetalle` VALUES (263, '27', '14', '9');
INSERT INTO `almacencontroldetalle` VALUES (264, '27', '15', '9');
INSERT INTO `almacencontroldetalle` VALUES (265, '27', '16', '9');
INSERT INTO `almacencontroldetalle` VALUES (266, '27', '17', '99');
INSERT INTO `almacencontroldetalle` VALUES (267, '27', '18', '99');
INSERT INTO `almacencontroldetalle` VALUES (268, '27', '19', '9999');
INSERT INTO `almacencontroldetalle` VALUES (269, '28', '1', '0');
INSERT INTO `almacencontroldetalle` VALUES (270, '28', '2', '0');
INSERT INTO `almacencontroldetalle` VALUES (271, '28', '3', '0');
INSERT INTO `almacencontroldetalle` VALUES (272, '28', '4', '0');
INSERT INTO `almacencontroldetalle` VALUES (273, '28', '5', '0');
INSERT INTO `almacencontroldetalle` VALUES (274, '28', '6', '0');
INSERT INTO `almacencontroldetalle` VALUES (275, '28', '7', '0');
INSERT INTO `almacencontroldetalle` VALUES (276, '28', '8', '0');
INSERT INTO `almacencontroldetalle` VALUES (277, '28', '9', '0');
INSERT INTO `almacencontroldetalle` VALUES (278, '28', '10', '0');
INSERT INTO `almacencontroldetalle` VALUES (279, '28', '11', '0');
INSERT INTO `almacencontroldetalle` VALUES (280, '28', '12', '0');
INSERT INTO `almacencontroldetalle` VALUES (281, '28', '13', '0');
INSERT INTO `almacencontroldetalle` VALUES (282, '28', '14', '0');
INSERT INTO `almacencontroldetalle` VALUES (283, '28', '15', '300');
INSERT INTO `almacencontroldetalle` VALUES (284, '28', '16', '0');
INSERT INTO `almacencontroldetalle` VALUES (285, '28', '17', '300');
INSERT INTO `almacencontroldetalle` VALUES (286, '28', '18', '0');
INSERT INTO `almacencontroldetalle` VALUES (287, '28', '19', '0');
INSERT INTO `almacencontroldetalle` VALUES (288, '29', '1', '20');
INSERT INTO `almacencontroldetalle` VALUES (289, '29', '19', '80');
INSERT INTO `almacencontroldetalle` VALUES (290, '30', '2', '20');
INSERT INTO `almacencontroldetalle` VALUES (291, '30', '4', '40');
INSERT INTO `almacencontroldetalle` VALUES (292, '30', '19', '80');
INSERT INTO `almacencontroldetalle` VALUES (293, '31', '2', '50');
INSERT INTO `almacencontroldetalle` VALUES (294, '31', '16', '50');
INSERT INTO `almacencontroldetalle` VALUES (295, '31', '17', '50');
INSERT INTO `almacencontroldetalle` VALUES (296, '31', '18', '50');
INSERT INTO `almacencontroldetalle` VALUES (297, '31', '19', '50');
INSERT INTO `almacencontroldetalle` VALUES (298, '32', '15', '1');
INSERT INTO `almacencontroldetalle` VALUES (299, '32', '16', '2');
INSERT INTO `almacencontroldetalle` VALUES (300, '32', '17', '3');
INSERT INTO `almacencontroldetalle` VALUES (301, '32', '18', '4');
INSERT INTO `almacencontroldetalle` VALUES (302, '32', '19', '5');
INSERT INTO `almacencontroldetalle` VALUES (303, '33', '1', '9');
INSERT INTO `almacencontroldetalle` VALUES (304, '33', '2', '8');
INSERT INTO `almacencontroldetalle` VALUES (305, '33', '3', '7');
INSERT INTO `almacencontroldetalle` VALUES (306, '33', '15', '1');
INSERT INTO `almacencontroldetalle` VALUES (307, '33', '16', '2');
INSERT INTO `almacencontroldetalle` VALUES (308, '33', '17', '3');
INSERT INTO `almacencontroldetalle` VALUES (309, '33', '18', '4');
INSERT INTO `almacencontroldetalle` VALUES (310, '33', '19', '5');
INSERT INTO `almacencontroldetalle` VALUES (311, '36', '1', '9');
INSERT INTO `almacencontroldetalle` VALUES (312, '36', '2', '8');
INSERT INTO `almacencontroldetalle` VALUES (313, '36', '3', '7');
INSERT INTO `almacencontroldetalle` VALUES (314, '36', '15', '1');
INSERT INTO `almacencontroldetalle` VALUES (315, '36', '16', '2');
INSERT INTO `almacencontroldetalle` VALUES (316, '36', '17', '3');
INSERT INTO `almacencontroldetalle` VALUES (317, '36', '18', '4');
INSERT INTO `almacencontroldetalle` VALUES (318, '36', '19', '5');
INSERT INTO `almacencontroldetalle` VALUES (319, '37', '15', '256');
INSERT INTO `almacencontroldetalle` VALUES (320, '38', '1', '50');
INSERT INTO `almacencontroldetalle` VALUES (321, '39', '1', '50');
INSERT INTO `almacencontroldetalle` VALUES (322, '40', '1', '200');
INSERT INTO `almacencontroldetalle` VALUES (323, '41', '1', '200');
INSERT INTO `almacencontroldetalle` VALUES (324, '42', '1', '50');
INSERT INTO `almacencontroldetalle` VALUES (325, '42', '2', '50');
INSERT INTO `almacencontroldetalle` VALUES (326, '42', '3', '50');
INSERT INTO `almacencontroldetalle` VALUES (327, '42', '4', '50');
INSERT INTO `almacencontroldetalle` VALUES (328, '42', '5', '50');
INSERT INTO `almacencontroldetalle` VALUES (329, '42', '6', '50');
INSERT INTO `almacencontroldetalle` VALUES (330, '42', '7', '50');
INSERT INTO `almacencontroldetalle` VALUES (331, '42', '8', '50');
INSERT INTO `almacencontroldetalle` VALUES (332, '42', '9', '50');
INSERT INTO `almacencontroldetalle` VALUES (333, '42', '10', '50');
INSERT INTO `almacencontroldetalle` VALUES (334, '42', '11', '50');
INSERT INTO `almacencontroldetalle` VALUES (335, '42', '12', '50');
INSERT INTO `almacencontroldetalle` VALUES (336, '42', '13', '50');
INSERT INTO `almacencontroldetalle` VALUES (337, '42', '14', '50');
INSERT INTO `almacencontroldetalle` VALUES (338, '42', '15', '50');
INSERT INTO `almacencontroldetalle` VALUES (339, '42', '16', '50');
INSERT INTO `almacencontroldetalle` VALUES (340, '42', '17', '50');
INSERT INTO `almacencontroldetalle` VALUES (341, '42', '18', '50');
INSERT INTO `almacencontroldetalle` VALUES (342, '42', '19', '50');
INSERT INTO `almacencontroldetalle` VALUES (343, '43', '1', '10');
INSERT INTO `almacencontroldetalle` VALUES (344, '44', '1', '10');
INSERT INTO `almacencontroldetalle` VALUES (345, '44', '15', '12');
INSERT INTO `almacencontroldetalle` VALUES (346, '44', '16', '12');
INSERT INTO `almacencontroldetalle` VALUES (347, '44', '17', '12');
INSERT INTO `almacencontroldetalle` VALUES (348, '45', '1', '200');
INSERT INTO `almacencontroldetalle` VALUES (349, '46', '2', '49');
INSERT INTO `almacencontroldetalle` VALUES (350, '46', '1', '89');
INSERT INTO `almacencontroldetalle` VALUES (351, '47', '1', '89');
INSERT INTO `almacencontroldetalle` VALUES (352, '51', '1', '20');
INSERT INTO `almacencontroldetalle` VALUES (353, '52', '17', '50');
INSERT INTO `almacencontroldetalle` VALUES (354, '53', '15', '6');
INSERT INTO `almacencontroldetalle` VALUES (355, '55', '9', '10');
INSERT INTO `almacencontroldetalle` VALUES (356, '55', '10', '10');
INSERT INTO `almacencontroldetalle` VALUES (357, '55', '11', '10');
INSERT INTO `almacencontroldetalle` VALUES (358, '55', '12', '10');
INSERT INTO `almacencontroldetalle` VALUES (359, '55', '13', '10');
INSERT INTO `almacencontroldetalle` VALUES (360, '55', '14', '10');
INSERT INTO `almacencontroldetalle` VALUES (361, '55', '15', '10');
INSERT INTO `almacencontroldetalle` VALUES (362, '55', '16', '10');
INSERT INTO `almacencontroldetalle` VALUES (363, '55', '17', '10');
INSERT INTO `almacencontroldetalle` VALUES (364, '55', '18', '10');
INSERT INTO `almacencontroldetalle` VALUES (365, '55', '19', '10');
INSERT INTO `almacencontroldetalle` VALUES (366, '55', '20', '10');
INSERT INTO `almacencontroldetalle` VALUES (367, '55', '21', '10');
INSERT INTO `almacencontroldetalle` VALUES (368, '56', '3', '44');
INSERT INTO `almacencontroldetalle` VALUES (369, '58', '1', '10');
INSERT INTO `almacencontroldetalle` VALUES (370, '58', '2', '10');
INSERT INTO `almacencontroldetalle` VALUES (371, '58', '3', '10');
INSERT INTO `almacencontroldetalle` VALUES (372, '58', '4', '10');
INSERT INTO `almacencontroldetalle` VALUES (373, '58', '5', '10');
INSERT INTO `almacencontroldetalle` VALUES (374, '58', '6', '10');
INSERT INTO `almacencontroldetalle` VALUES (375, '58', '7', '10');
INSERT INTO `almacencontroldetalle` VALUES (376, '58', '8', '10');
INSERT INTO `almacencontroldetalle` VALUES (377, '59', '5', '1');
INSERT INTO `almacencontroldetalle` VALUES (378, '59', '6', '1');
INSERT INTO `almacencontroldetalle` VALUES (379, '60', '1', '1');
INSERT INTO `almacencontroldetalle` VALUES (380, '60', '2', '1');
INSERT INTO `almacencontroldetalle` VALUES (381, '60', '8', '1');
INSERT INTO `almacencontroldetalle` VALUES (382, '61', '9', '10');
INSERT INTO `almacencontroldetalle` VALUES (383, '61', '10', '10');
INSERT INTO `almacencontroldetalle` VALUES (384, '61', '11', '10');
INSERT INTO `almacencontroldetalle` VALUES (385, '61', '12', '10');
INSERT INTO `almacencontroldetalle` VALUES (386, '61', '13', '10');
INSERT INTO `almacencontroldetalle` VALUES (387, '61', '14', '10');
INSERT INTO `almacencontroldetalle` VALUES (388, '61', '15', '10');
INSERT INTO `almacencontroldetalle` VALUES (389, '61', '16', '10');
INSERT INTO `almacencontroldetalle` VALUES (390, '61', '17', '10');
INSERT INTO `almacencontroldetalle` VALUES (391, '61', '18', '10');
INSERT INTO `almacencontroldetalle` VALUES (392, '61', '19', '10');
INSERT INTO `almacencontroldetalle` VALUES (393, '61', '20', '10');
INSERT INTO `almacencontroldetalle` VALUES (394, '62', '13', '9');
INSERT INTO `almacencontroldetalle` VALUES (395, '63', '9', '8');
INSERT INTO `almacencontroldetalle` VALUES (396, '64', '10', '6');
INSERT INTO `almacencontroldetalle` VALUES (397, '64', '12', '6');
INSERT INTO `almacencontroldetalle` VALUES (398, '64', '13', '19');
INSERT INTO `almacencontroldetalle` VALUES (399, '64', '15', '4');
INSERT INTO `almacencontroldetalle` VALUES (400, '64', '17', '25');
INSERT INTO `almacencontroldetalle` VALUES (401, '64', '18', '12');
INSERT INTO `almacencontroldetalle` VALUES (402, '64', '19', '3');
INSERT INTO `almacencontroldetalle` VALUES (403, '66', '1', '10');
INSERT INTO `almacencontroldetalle` VALUES (404, '66', '2', '10');
INSERT INTO `almacencontroldetalle` VALUES (405, '66', '3', '10');
INSERT INTO `almacencontroldetalle` VALUES (406, '66', '4', '5');
INSERT INTO `almacencontroldetalle` VALUES (407, '66', '5', '10');
INSERT INTO `almacencontroldetalle` VALUES (408, '66', '6', '10');
INSERT INTO `almacencontroldetalle` VALUES (409, '66', '7', '10');
INSERT INTO `almacencontroldetalle` VALUES (410, '66', '8', '10');
INSERT INTO `almacencontroldetalle` VALUES (411, '68', '18', '10');
INSERT INTO `almacencontroldetalle` VALUES (412, '69', '19', '1');
INSERT INTO `almacencontroldetalle` VALUES (413, '70', '9', '3');
INSERT INTO `almacencontroldetalle` VALUES (414, '71', '1', '10');
INSERT INTO `almacencontroldetalle` VALUES (415, '71', '2', '10');
INSERT INTO `almacencontroldetalle` VALUES (416, '71', '3', '10');
INSERT INTO `almacencontroldetalle` VALUES (417, '71', '4', '10');
INSERT INTO `almacencontroldetalle` VALUES (418, '71', '5', '10');
INSERT INTO `almacencontroldetalle` VALUES (419, '71', '6', '10');
INSERT INTO `almacencontroldetalle` VALUES (420, '71', '7', '10');
INSERT INTO `almacencontroldetalle` VALUES (421, '71', '8', '10');
INSERT INTO `almacencontroldetalle` VALUES (422, '71', '9', '3');
INSERT INTO `almacencontroldetalle` VALUES (423, '71', '10', '7');
INSERT INTO `almacencontroldetalle` VALUES (424, '71', '11', '3');
INSERT INTO `almacencontroldetalle` VALUES (425, '71', '12', '12');
INSERT INTO `almacencontroldetalle` VALUES (426, '71', '13', '17');
INSERT INTO `almacencontroldetalle` VALUES (427, '71', '14', '9');
INSERT INTO `almacencontroldetalle` VALUES (428, '71', '15', '3');
INSERT INTO `almacencontroldetalle` VALUES (429, '71', '16', '7');
INSERT INTO `almacencontroldetalle` VALUES (430, '71', '17', '15');
INSERT INTO `almacencontroldetalle` VALUES (431, '71', '18', '6');
INSERT INTO `almacencontroldetalle` VALUES (432, '71', '19', '8');
INSERT INTO `almacencontroldetalle` VALUES (433, '71', '20', '8');
INSERT INTO `almacencontroldetalle` VALUES (434, '71', '21', '2');
INSERT INTO `almacencontroldetalle` VALUES (435, '71', '22', '6');
INSERT INTO `almacencontroldetalle` VALUES (436, '71', '23', '5');
INSERT INTO `almacencontroldetalle` VALUES (437, '72', '21', '1');
INSERT INTO `almacencontroldetalle` VALUES (438, '73', '21', '3');
INSERT INTO `almacencontroldetalle` VALUES (439, '74', '14', '2');
INSERT INTO `almacencontroldetalle` VALUES (440, '76', '1', '19');
INSERT INTO `almacencontroldetalle` VALUES (441, '76', '6', '73');
INSERT INTO `almacencontroldetalle` VALUES (442, '76', '7', '19');
INSERT INTO `almacencontroldetalle` VALUES (443, '76', '8', '5');
INSERT INTO `almacencontroldetalle` VALUES (444, '77', '19', '5');
INSERT INTO `almacencontroldetalle` VALUES (445, '77', '20', '5');
INSERT INTO `almacencontroldetalle` VALUES (446, '78', '14', '20');
INSERT INTO `almacencontroldetalle` VALUES (447, '79', '9', '3');
INSERT INTO `almacencontroldetalle` VALUES (448, '79', '11', '11');
INSERT INTO `almacencontroldetalle` VALUES (449, '79', '14', '20');
INSERT INTO `almacencontroldetalle` VALUES (450, '79', '17', '20');
INSERT INTO `almacencontroldetalle` VALUES (451, '79', '19', '4');
INSERT INTO `almacencontroldetalle` VALUES (452, '79', '21', '1');
INSERT INTO `almacencontroldetalle` VALUES (453, '79', '23', '1');
INSERT INTO `almacencontroldetalle` VALUES (454, '80', '18', '24');
INSERT INTO `almacencontroldetalle` VALUES (455, '81', '23', '2');
INSERT INTO `almacencontroldetalle` VALUES (456, '82', '22', '3');
INSERT INTO `almacencontroldetalle` VALUES (457, '83', '21', '2');
INSERT INTO `almacencontroldetalle` VALUES (458, '84', '18', '5');
INSERT INTO `almacencontroldetalle` VALUES (459, '84', '19', '15');
INSERT INTO `almacencontroldetalle` VALUES (460, '86', '10', '5');
INSERT INTO `almacencontroldetalle` VALUES (461, '86', '13', '6');
INSERT INTO `almacencontroldetalle` VALUES (462, '86', '21', '3');
INSERT INTO `almacencontroldetalle` VALUES (463, '87', '1', '100');
INSERT INTO `almacencontroldetalle` VALUES (464, '87', '2', '20');
INSERT INTO `almacencontroldetalle` VALUES (465, '87', '3', '10');
INSERT INTO `almacencontroldetalle` VALUES (466, '88', '1', '20');
INSERT INTO `almacencontroldetalle` VALUES (467, '88', '2', '50');
INSERT INTO `almacencontroldetalle` VALUES (468, '88', '3', '10');
INSERT INTO `almacencontroldetalle` VALUES (469, '88', '4', '10');
INSERT INTO `almacencontroldetalle` VALUES (470, '89', '5', '1');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `carta`
-- 

CREATE TABLE `carta` (
  `idCarta` mediumint(4) NOT NULL auto_increment,
  `cCodigo` varchar(4) NOT NULL,
  `cDescripcion` varchar(500) NOT NULL,
  `cFecha` datetime NOT NULL,
  `idUsuario` varchar(4) NOT NULL,
  PRIMARY KEY  (`idCarta`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `carta`
-- 

INSERT INTO `carta` VALUES (1, '1', 'CARTA PARA DIA DE LA MADRE', '2015-01-22 11:31:07', '1');
INSERT INTO `carta` VALUES (2, '', '', '0000-00-00 00:00:00', '');
INSERT INTO `carta` VALUES (3, '', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `familia`
-- 

CREATE TABLE `familia` (
  `fId` mediumint(5) NOT NULL auto_increment,
  `fNombre` varchar(200) NOT NULL,
  `fDescripcion` varchar(200) NOT NULL,
  `fFamilia` char(2) NOT NULL,
  `fCategoria` varchar(200) NOT NULL,
  `fThumb` varchar(50) NOT NULL,
  PRIMARY KEY  (`fId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- 
-- Volcar la base de datos para la tabla `familia`
-- 

INSERT INTO `familia` VALUES (1, 'Makis', 'Makis', '1', '1', 'makisushi.jpg');
INSERT INTO `familia` VALUES (2, 'Sushi', 'Sushi', '2', '1', 'salchipapas.jpg');
INSERT INTO `familia` VALUES (3, 'Sashimi', 'Gaseosas', '3', '1', 'sashimi.jpg');
INSERT INTO `familia` VALUES (4, 'Bebidas', 'Bebidas', '4', '3', 'Bebidas.jpg');
INSERT INTO `familia` VALUES (5, 'Cocteles de La Casa', 'Cocteles de la casa', '5', '3', 'cocteles.jpg');
INSERT INTO `familia` VALUES (6, 'Chilcanos', 'chilcanos', '6', '3', 'chilcanos.jpg');
INSERT INTO `familia` VALUES (7, 'Sour''s', 'Sour', '7', '3', 'sours.jpg');
INSERT INTO `familia` VALUES (8, 'Cocteles Virgenes', 'cocteles virgenes', '8', '3', 'coctelesvirgenes.jpg');
INSERT INTO `familia` VALUES (9, 'Otros', 'Otros', '9', '3', 'otros.jpg');
INSERT INTO `familia` VALUES (10, 'Calientes', 'Calientes para el Frio', '10', '3', 'calientitos.jpg');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `mesa`
-- 

CREATE TABLE `mesa` (
  `mId` mediumint(3) NOT NULL auto_increment,
  `mNumero` char(9) NOT NULL,
  `mFamilia` char(3) NOT NULL,
  `mEstado` varchar(20) NOT NULL,
  `mImg` varchar(20) NOT NULL,
  `mFecha` datetime NOT NULL,
  `mPago` varchar(5) NOT NULL,
  `fk_persona` int(11) default NULL,
  PRIMARY KEY  (`mId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

-- 
-- Volcar la base de datos para la tabla `mesa`
-- 

INSERT INTO `mesa` VALUES (1, '1', '1', '0', 'mesa1.png', '2021-10-07 00:50:02', '0', NULL);
INSERT INTO `mesa` VALUES (2, '2', '2', '0', 'mesa1.png', '2021-09-25 16:35:37', '0', NULL);
INSERT INTO `mesa` VALUES (3, '3', '3', '0', 'mesa1.png', '2021-09-25 16:35:44', '0', NULL);
INSERT INTO `mesa` VALUES (4, '4', '4', '0', 'mesa1.png', '2021-09-15 22:18:05', '0', NULL);
INSERT INTO `mesa` VALUES (5, '5', '5', '0', 'mesa1.png', '2021-10-05 23:53:05', '0', NULL);
INSERT INTO `mesa` VALUES (6, '6', '6', '0', 'mesa1.png', '2020-08-12 17:47:44', '0', NULL);
INSERT INTO `mesa` VALUES (7, '7', '7', '0', 'mesa1.png', '2017-02-11 17:22:06', '0', NULL);
INSERT INTO `mesa` VALUES (8, '8', '8', '0', 'mesa1.png', '2017-02-19 14:08:17', '0', NULL);
INSERT INTO `mesa` VALUES (9, '9', '9', '0', 'mesa1.png', '2017-02-19 13:33:05', '0', NULL);
INSERT INTO `mesa` VALUES (10, '10', '10', '0', 'mesa1.png', '2017-02-19 14:13:04', '0', NULL);
INSERT INTO `mesa` VALUES (11, '11', '11', '0', 'mesa1.png', '2021-09-04 18:00:57', '0', NULL);
INSERT INTO `mesa` VALUES (12, '12', '12', '0', 'mesa1.png', '2021-07-17 16:26:22', '0', NULL);
INSERT INTO `mesa` VALUES (13, '13', '13', '0', 'mesa1.png', '2021-10-23 18:55:13', '0', NULL);
INSERT INTO `mesa` VALUES (14, '14', '14', '0', 'mesa1.png', '2021-10-23 19:11:15', '1', NULL);
INSERT INTO `mesa` VALUES (15, '15', '15', '0', 'mesa1.png', '2017-02-19 14:49:30', '0', NULL);
INSERT INTO `mesa` VALUES (16, '16', '16', '0', 'mesa1.png', '2017-01-30 15:55:59', '0', NULL);
INSERT INTO `mesa` VALUES (17, '17', '17', '0', 'mesa1.png', '2017-02-19 14:54:18', '0', NULL);
INSERT INTO `mesa` VALUES (18, '18', '18', '0', 'mesa1.png', '2017-01-31 14:12:14', '0', NULL);
INSERT INTO `mesa` VALUES (19, '19', '19', '0', 'mesa1.png', '2017-02-19 13:48:58', '0', NULL);
INSERT INTO `mesa` VALUES (20, '20', '20', '0', 'mesa1.png', '2017-02-14 15:42:53', '0', NULL);
INSERT INTO `mesa` VALUES (21, '21', '21', '0', 'mesa1.png', '2017-02-19 16:56:41', '0', NULL);
INSERT INTO `mesa` VALUES (22, '22', '22', '0', 'mesa1.png', '2017-02-19 17:05:54', '0', NULL);
INSERT INTO `mesa` VALUES (23, '23', '23', '0', 'mesa1.png', '2017-02-11 17:01:32', '0', NULL);
INSERT INTO `mesa` VALUES (24, '24', '24', '0', 'mesa1.png', '2017-02-19 16:27:50', '0', NULL);
INSERT INTO `mesa` VALUES (25, '25', '25', '0', 'mesa1.png', '2017-02-11 13:32:38', '0', NULL);
INSERT INTO `mesa` VALUES (26, '26', '26', '0', 'mesa1.png', '2017-02-19 14:11:55', '0', NULL);
INSERT INTO `mesa` VALUES (27, '27', '27', '0', 'mesa1.png', '2017-02-19 13:26:50', '0', NULL);
INSERT INTO `mesa` VALUES (28, '28', '28', '0', 'mesa1.png', '2017-02-19 16:44:28', '0', NULL);
INSERT INTO `mesa` VALUES (29, '29', '29', '0', 'mesa1.png', '2017-02-18 14:58:56', '0', NULL);
INSERT INTO `mesa` VALUES (30, '30', '30', '0', 'mesa1.png', '2017-02-19 15:43:30', '0', NULL);
INSERT INTO `mesa` VALUES (31, '31', '31', '1', 'mesa1.png', '2021-10-26 01:31:11', '1', 5);
INSERT INTO `mesa` VALUES (32, '32', '32', '1', 'mesa1.png', '2021-11-21 11:36:16', '1', 5);
INSERT INTO `mesa` VALUES (33, '33', '33', '1', 'mesa1.png', '2021-11-21 16:02:31', '1', 5);
INSERT INTO `mesa` VALUES (34, '34', '34', '0', 'mesa1.png', '2017-01-31 14:07:27', '0', 5);
INSERT INTO `mesa` VALUES (35, '35', '35', '0', 'mesa1.png', '2015-05-14 21:50:14', '0', 5);
INSERT INTO `mesa` VALUES (36, '36', '36', '0', 'mesa1.png', '0000-00-00 00:00:00', '0', 5);
INSERT INTO `mesa` VALUES (37, '37', '37', '0', 'mesa1.png', '2017-01-31 14:11:07', '0', 5);
INSERT INTO `mesa` VALUES (38, '38', '38', '0', 'mesa1.png', '0000-00-00 00:00:00', '0', 5);
INSERT INTO `mesa` VALUES (39, '39', '39', '0', 'mesa1.png', '2017-02-19 17:10:56', '0', 5);
INSERT INTO `mesa` VALUES (40, '40', '40', '0', 'mesa1.png', '2017-02-19 15:45:28', '0', 5);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `nota_venta`
-- 

CREATE TABLE `nota_venta` (
  `nId` int(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Volcar la base de datos para la tabla `nota_venta`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `persona`
-- 

CREATE TABLE `persona` (
  `pId` mediumint(3) NOT NULL auto_increment,
  `pNombres` varchar(50) NOT NULL,
  `pApellidos` varchar(50) NOT NULL,
  `pFechNac` date NOT NULL,
  `pSexo` varchar(50) NOT NULL,
  `pTelCell` varchar(50) NOT NULL,
  `pImg` varchar(50) NOT NULL,
  `pCargo` double(3,0) NOT NULL,
  `pNomCorto` varchar(50) NOT NULL,
  `pAlias` varchar(50) NOT NULL,
  `pPassword` varchar(50) NOT NULL,
  `pFechReg` datetime NOT NULL,
  PRIMARY KEY  (`pId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- 
-- Volcar la base de datos para la tabla `persona`
-- 

INSERT INTO `persona` VALUES (1, 'NUEVO ', 'PERSONAL', '2017-02-12', 'M', '999999999', 'per1.jpg', 1, 'Nuevo', 'Nuevo', '123456', '0000-00-00 00:00:00');
INSERT INTO `persona` VALUES (2, 'Luz', 'Jurado', '2017-02-12', 'F', '999999999', 'per7.jpg', 2, 'Luz', 'Luz', '123456', '0000-00-00 00:00:00');
INSERT INTO `persona` VALUES (3, 'VICTOR', 'ViCTOR', '2017-02-12', 'M', '999999999', 'per7.jpg', 3, '', 'Victor', '123456', '2015-05-06 00:00:00');
INSERT INTO `persona` VALUES (4, 'THALIA', 'XXXXXXX', '2017-02-12', 'F', '999999999', 'per7.jpg', 4, '', 'thalia', '123456', '2015-05-06 00:00:00');
INSERT INTO `persona` VALUES (5, 'YHADIRA', 'XXXXXX', '2017-02-12', 'M', '999999999', 'per7.jpg', 0, '', 'Nuevo', '', '2015-05-06 00:00:00');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `pid`
-- 

CREATE TABLE `pid` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `saleId` int(11) NOT NULL,
  `status` varchar(32) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=388 ;

-- 
-- Volcar la base de datos para la tabla `pid`
-- 

INSERT INTO `pid` VALUES (1, 1, 'PRI');
INSERT INTO `pid` VALUES (2, 1, 'PRI');
INSERT INTO `pid` VALUES (3, 2, 'PRI');
INSERT INTO `pid` VALUES (4, 2, 'PRI');
INSERT INTO `pid` VALUES (5, 2, 'PRI');
INSERT INTO `pid` VALUES (6, 2, 'PRI');
INSERT INTO `pid` VALUES (7, 14, 'PRI');
INSERT INTO `pid` VALUES (8, 15, 'PRI');
INSERT INTO `pid` VALUES (9, 15, 'PRI');
INSERT INTO `pid` VALUES (10, 15, 'PRI');
INSERT INTO `pid` VALUES (11, 16, 'PRI');
INSERT INTO `pid` VALUES (12, 16, 'PRI');
INSERT INTO `pid` VALUES (13, 16, 'PRI');
INSERT INTO `pid` VALUES (14, 16, 'PRI');
INSERT INTO `pid` VALUES (15, 15, 'PRI');
INSERT INTO `pid` VALUES (16, 15, 'PRI');
INSERT INTO `pid` VALUES (17, 17, 'PRI');
INSERT INTO `pid` VALUES (18, 17, 'PRI');
INSERT INTO `pid` VALUES (19, 17, 'PRI');
INSERT INTO `pid` VALUES (20, 17, 'PRI');
INSERT INTO `pid` VALUES (21, 18, 'PRI');
INSERT INTO `pid` VALUES (22, 18, 'PRI');
INSERT INTO `pid` VALUES (23, 18, 'PRI');
INSERT INTO `pid` VALUES (24, 18, 'PRI');
INSERT INTO `pid` VALUES (25, 19, 'PRI');
INSERT INTO `pid` VALUES (26, 19, 'PRI');
INSERT INTO `pid` VALUES (27, 20, 'PRI');
INSERT INTO `pid` VALUES (28, 20, 'PRI');
INSERT INTO `pid` VALUES (29, 20, 'PRI');
INSERT INTO `pid` VALUES (30, 20, 'PRI');
INSERT INTO `pid` VALUES (31, 21, 'PRI');
INSERT INTO `pid` VALUES (32, 21, 'PRI');
INSERT INTO `pid` VALUES (33, 21, 'PRI');
INSERT INTO `pid` VALUES (34, 21, 'PRI');
INSERT INTO `pid` VALUES (35, 21, 'PRI');
INSERT INTO `pid` VALUES (36, 21, 'PRI');
INSERT INTO `pid` VALUES (37, 22, 'PRI');
INSERT INTO `pid` VALUES (38, 22, 'PRI');
INSERT INTO `pid` VALUES (39, 22, 'PRI');
INSERT INTO `pid` VALUES (40, 22, 'PRI');
INSERT INTO `pid` VALUES (41, 22, 'PRI');
INSERT INTO `pid` VALUES (42, 23, 'PRI');
INSERT INTO `pid` VALUES (43, 23, 'PRI');
INSERT INTO `pid` VALUES (44, 24, 'PRI');
INSERT INTO `pid` VALUES (45, 24, 'PRI');
INSERT INTO `pid` VALUES (46, 24, 'PRI');
INSERT INTO `pid` VALUES (47, 15, 'PRI');
INSERT INTO `pid` VALUES (48, 8, 'PRI');
INSERT INTO `pid` VALUES (49, 25, 'PRI');
INSERT INTO `pid` VALUES (50, 26, 'PRI');
INSERT INTO `pid` VALUES (51, 27, 'PRI');
INSERT INTO `pid` VALUES (52, 27, 'PRI');
INSERT INTO `pid` VALUES (53, 28, 'PRI');
INSERT INTO `pid` VALUES (54, 28, 'PRI');
INSERT INTO `pid` VALUES (55, 27, 'PRI');
INSERT INTO `pid` VALUES (56, 26, 'PRI');
INSERT INTO `pid` VALUES (57, 25, 'PRI');
INSERT INTO `pid` VALUES (58, 28, 'PRI');
INSERT INTO `pid` VALUES (59, 1, 'PRI');
INSERT INTO `pid` VALUES (60, 3, 'PRI');
INSERT INTO `pid` VALUES (61, 3, 'PRI');
INSERT INTO `pid` VALUES (62, 4, 'PRI');
INSERT INTO `pid` VALUES (63, 28, 'PRI');
INSERT INTO `pid` VALUES (64, 29, 'PRI');
INSERT INTO `pid` VALUES (65, 29, 'PRI');
INSERT INTO `pid` VALUES (66, 30, 'PRI');
INSERT INTO `pid` VALUES (67, 31, 'PRI');
INSERT INTO `pid` VALUES (68, 29, 'PRI');
INSERT INTO `pid` VALUES (69, 1, 'PRI');
INSERT INTO `pid` VALUES (70, 2, 'PRI');
INSERT INTO `pid` VALUES (71, 3, 'PRI');
INSERT INTO `pid` VALUES (72, 4, 'PRI');
INSERT INTO `pid` VALUES (73, 5, 'PRI');
INSERT INTO `pid` VALUES (74, 6, 'PRI');
INSERT INTO `pid` VALUES (75, 7, 'PRI');
INSERT INTO `pid` VALUES (76, 8, 'PRI');
INSERT INTO `pid` VALUES (77, 9, 'PRI');
INSERT INTO `pid` VALUES (78, 10, 'PRI');
INSERT INTO `pid` VALUES (79, 11, 'PRI');
INSERT INTO `pid` VALUES (80, 12, 'PRI');
INSERT INTO `pid` VALUES (81, 13, 'PRI');
INSERT INTO `pid` VALUES (82, 14, 'PRI');
INSERT INTO `pid` VALUES (83, 15, 'PRI');
INSERT INTO `pid` VALUES (84, 16, 'PRI');
INSERT INTO `pid` VALUES (85, 17, 'PRI');
INSERT INTO `pid` VALUES (86, 18, 'PRI');
INSERT INTO `pid` VALUES (87, 15, 'PRI');
INSERT INTO `pid` VALUES (88, 19, 'PRI');
INSERT INTO `pid` VALUES (89, 20, 'PRI');
INSERT INTO `pid` VALUES (90, 21, 'PRI');
INSERT INTO `pid` VALUES (91, 22, 'PRI');
INSERT INTO `pid` VALUES (92, 23, 'PRI');
INSERT INTO `pid` VALUES (93, 24, 'PRI');
INSERT INTO `pid` VALUES (94, 1, 'PRI');
INSERT INTO `pid` VALUES (95, 2, 'PRI');
INSERT INTO `pid` VALUES (96, 3, 'PRI');
INSERT INTO `pid` VALUES (97, 4, 'PRI');
INSERT INTO `pid` VALUES (98, 4, 'PRI');
INSERT INTO `pid` VALUES (99, 5, 'PRI');
INSERT INTO `pid` VALUES (100, 6, 'PRI');
INSERT INTO `pid` VALUES (101, 7, 'PRI');
INSERT INTO `pid` VALUES (102, 8, 'PRI');
INSERT INTO `pid` VALUES (103, 8, 'PRI');
INSERT INTO `pid` VALUES (104, 9, 'PRI');
INSERT INTO `pid` VALUES (105, 9, 'PRI');
INSERT INTO `pid` VALUES (106, 10, 'PRI');
INSERT INTO `pid` VALUES (107, 9, 'PRI');
INSERT INTO `pid` VALUES (108, 9, 'PRI');
INSERT INTO `pid` VALUES (109, 11, 'PRI');
INSERT INTO `pid` VALUES (110, 10, 'PRI');
INSERT INTO `pid` VALUES (111, 12, 'PRI');
INSERT INTO `pid` VALUES (112, 10, 'PRI');
INSERT INTO `pid` VALUES (113, 11, 'PRI');
INSERT INTO `pid` VALUES (114, 13, 'PRI');
INSERT INTO `pid` VALUES (115, 14, 'PRI');
INSERT INTO `pid` VALUES (116, 13, 'PRI');
INSERT INTO `pid` VALUES (117, 9, 'PRI');
INSERT INTO `pid` VALUES (118, 11, 'PRI');
INSERT INTO `pid` VALUES (119, 15, 'PRI');
INSERT INTO `pid` VALUES (120, 11, 'PRI');
INSERT INTO `pid` VALUES (121, 15, 'PRI');
INSERT INTO `pid` VALUES (122, 10, 'PRI');
INSERT INTO `pid` VALUES (123, 13, 'PRI');
INSERT INTO `pid` VALUES (124, 16, 'PRI');
INSERT INTO `pid` VALUES (125, 13, 'PRI');
INSERT INTO `pid` VALUES (126, 13, 'PRI');
INSERT INTO `pid` VALUES (127, 11, 'PRI');
INSERT INTO `pid` VALUES (128, 9, 'PRI');
INSERT INTO `pid` VALUES (129, 9, 'PRI');
INSERT INTO `pid` VALUES (130, 9, 'PRI');
INSERT INTO `pid` VALUES (131, 17, 'PRI');
INSERT INTO `pid` VALUES (132, 17, 'PRI');
INSERT INTO `pid` VALUES (133, 17, 'PRI');
INSERT INTO `pid` VALUES (134, 16, 'PRI');
INSERT INTO `pid` VALUES (135, 18, 'PRI');
INSERT INTO `pid` VALUES (136, 18, 'PRI');
INSERT INTO `pid` VALUES (137, 11, 'PRI');
INSERT INTO `pid` VALUES (138, 17, 'PRI');
INSERT INTO `pid` VALUES (139, 11, 'PRI');
INSERT INTO `pid` VALUES (140, 19, 'PRI');
INSERT INTO `pid` VALUES (141, 20, 'PRI');
INSERT INTO `pid` VALUES (142, 20, 'PRI');
INSERT INTO `pid` VALUES (143, 21, 'PRI');
INSERT INTO `pid` VALUES (144, 22, 'PRI');
INSERT INTO `pid` VALUES (145, 17, 'PRI');
INSERT INTO `pid` VALUES (146, 17, 'PRI');
INSERT INTO `pid` VALUES (147, 22, 'PRI');
INSERT INTO `pid` VALUES (148, 23, 'PRI');
INSERT INTO `pid` VALUES (149, 24, 'PRI');
INSERT INTO `pid` VALUES (150, 13, 'PRI');
INSERT INTO `pid` VALUES (151, 25, 'PRI');
INSERT INTO `pid` VALUES (152, 26, 'PRI');
INSERT INTO `pid` VALUES (153, 27, 'PRI');
INSERT INTO `pid` VALUES (154, 28, 'PRI');
INSERT INTO `pid` VALUES (155, 17, 'PRI');
INSERT INTO `pid` VALUES (156, 29, 'PRI');
INSERT INTO `pid` VALUES (157, 30, 'PRI');
INSERT INTO `pid` VALUES (158, 8, 'PRI');
INSERT INTO `pid` VALUES (159, 8, 'PRI');
INSERT INTO `pid` VALUES (160, 9, 'PRI');
INSERT INTO `pid` VALUES (161, 10, 'PRI');
INSERT INTO `pid` VALUES (162, 11, 'PRI');
INSERT INTO `pid` VALUES (163, 12, 'PRI');
INSERT INTO `pid` VALUES (164, 13, 'PRI');
INSERT INTO `pid` VALUES (165, 14, 'PRI');
INSERT INTO `pid` VALUES (166, 11, 'PRI');
INSERT INTO `pid` VALUES (167, 14, 'PRI');
INSERT INTO `pid` VALUES (168, 12, 'PRI');
INSERT INTO `pid` VALUES (169, 15, 'PRI');
INSERT INTO `pid` VALUES (170, 16, 'PRI');
INSERT INTO `pid` VALUES (171, 12, 'PRI');
INSERT INTO `pid` VALUES (172, 13, 'PRI');
INSERT INTO `pid` VALUES (173, 17, 'PRI');
INSERT INTO `pid` VALUES (174, 18, 'PRI');
INSERT INTO `pid` VALUES (175, 19, 'PRI');
INSERT INTO `pid` VALUES (176, 18, 'PRI');
INSERT INTO `pid` VALUES (177, 20, 'PRI');
INSERT INTO `pid` VALUES (178, 21, 'PRI');
INSERT INTO `pid` VALUES (179, 20, 'PRI');
INSERT INTO `pid` VALUES (180, 20, 'PRI');
INSERT INTO `pid` VALUES (181, 17, 'PRI');
INSERT INTO `pid` VALUES (182, 22, 'PRI');
INSERT INTO `pid` VALUES (183, 23, 'PRI');
INSERT INTO `pid` VALUES (184, 23, 'PRI');
INSERT INTO `pid` VALUES (185, 23, 'PRI');
INSERT INTO `pid` VALUES (186, 24, 'PRI');
INSERT INTO `pid` VALUES (187, 23, 'PRI');
INSERT INTO `pid` VALUES (188, 24, 'PRI');
INSERT INTO `pid` VALUES (189, 25, 'PRI');
INSERT INTO `pid` VALUES (190, 23, 'PRI');
INSERT INTO `pid` VALUES (191, 23, 'PRI');
INSERT INTO `pid` VALUES (192, 26, 'PRI');
INSERT INTO `pid` VALUES (193, 25, 'PRI');
INSERT INTO `pid` VALUES (194, 27, 'PRI');
INSERT INTO `pid` VALUES (195, 27, 'PRI');
INSERT INTO `pid` VALUES (196, 28, 'PRI');
INSERT INTO `pid` VALUES (197, 28, 'PRI');
INSERT INTO `pid` VALUES (198, 26, 'PRI');
INSERT INTO `pid` VALUES (199, 26, 'PRI');
INSERT INTO `pid` VALUES (200, 28, 'PRI');
INSERT INTO `pid` VALUES (201, 29, 'PRI');
INSERT INTO `pid` VALUES (202, 29, 'PRI');
INSERT INTO `pid` VALUES (203, 29, 'PRI');
INSERT INTO `pid` VALUES (204, 30, 'PRI');
INSERT INTO `pid` VALUES (205, 31, 'PRI');
INSERT INTO `pid` VALUES (206, 32, 'PRI');
INSERT INTO `pid` VALUES (207, 33, 'PRI');
INSERT INTO `pid` VALUES (208, 33, 'PRI');
INSERT INTO `pid` VALUES (209, 31, 'PRI');
INSERT INTO `pid` VALUES (210, 31, 'PRI');
INSERT INTO `pid` VALUES (211, 34, 'PRI');
INSERT INTO `pid` VALUES (212, 33, 'PRI');
INSERT INTO `pid` VALUES (213, 33, 'PRI');
INSERT INTO `pid` VALUES (214, 35, 'PRI');
INSERT INTO `pid` VALUES (215, 33, 'PRI');
INSERT INTO `pid` VALUES (216, 32, 'PRI');
INSERT INTO `pid` VALUES (217, 36, 'PRI');
INSERT INTO `pid` VALUES (218, 33, 'PRI');
INSERT INTO `pid` VALUES (219, 33, 'PRI');
INSERT INTO `pid` VALUES (220, 37, 'PRI');
INSERT INTO `pid` VALUES (221, 38, 'PRI');
INSERT INTO `pid` VALUES (222, 39, 'PRI');
INSERT INTO `pid` VALUES (223, 40, 'PRI');
INSERT INTO `pid` VALUES (224, 41, 'PRI');
INSERT INTO `pid` VALUES (225, 40, 'PRI');
INSERT INTO `pid` VALUES (226, 40, 'PRI');
INSERT INTO `pid` VALUES (227, 42, 'PRI');
INSERT INTO `pid` VALUES (228, 43, 'PRI');
INSERT INTO `pid` VALUES (229, 43, 'PRI');
INSERT INTO `pid` VALUES (230, 44, 'PRI');
INSERT INTO `pid` VALUES (231, 44, 'PRI');
INSERT INTO `pid` VALUES (232, 44, 'PRI');
INSERT INTO `pid` VALUES (233, 45, 'PRI');
INSERT INTO `pid` VALUES (234, 45, 'PRI');
INSERT INTO `pid` VALUES (235, 44, 'PRI');
INSERT INTO `pid` VALUES (236, 46, 'PRI');
INSERT INTO `pid` VALUES (237, 47, 'PRI');
INSERT INTO `pid` VALUES (238, 47, 'PRI');
INSERT INTO `pid` VALUES (239, 48, 'PRI');
INSERT INTO `pid` VALUES (240, 49, 'PRI');
INSERT INTO `pid` VALUES (241, 48, 'PRI');
INSERT INTO `pid` VALUES (242, 49, 'PRI');
INSERT INTO `pid` VALUES (243, 50, 'PRI');
INSERT INTO `pid` VALUES (244, 50, 'PRI');
INSERT INTO `pid` VALUES (245, 51, 'PRI');
INSERT INTO `pid` VALUES (246, 52, 'PRI');
INSERT INTO `pid` VALUES (247, 52, 'PRI');
INSERT INTO `pid` VALUES (248, 51, 'PRI');
INSERT INTO `pid` VALUES (249, 53, 'PRI');
INSERT INTO `pid` VALUES (250, 51, 'PRI');
INSERT INTO `pid` VALUES (251, 51, 'PRI');
INSERT INTO `pid` VALUES (252, 54, 'PRI');
INSERT INTO `pid` VALUES (253, 51, 'PRI');
INSERT INTO `pid` VALUES (254, 50, 'PRI');
INSERT INTO `pid` VALUES (255, 55, 'PRI');
INSERT INTO `pid` VALUES (256, 55, 'PRI');
INSERT INTO `pid` VALUES (257, 51, 'PRI');
INSERT INTO `pid` VALUES (258, 55, 'PRI');
INSERT INTO `pid` VALUES (259, 55, 'PRI');
INSERT INTO `pid` VALUES (260, 55, 'PRI');
INSERT INTO `pid` VALUES (261, 56, 'PRI');
INSERT INTO `pid` VALUES (262, 57, 'PRI');
INSERT INTO `pid` VALUES (263, 55, 'PRI');
INSERT INTO `pid` VALUES (264, 58, 'PRI');
INSERT INTO `pid` VALUES (265, 51, 'PRI');
INSERT INTO `pid` VALUES (266, 47, 'PRI');
INSERT INTO `pid` VALUES (267, 55, 'PRI');
INSERT INTO `pid` VALUES (268, 59, 'PRI');
INSERT INTO `pid` VALUES (269, 60, 'PRI');
INSERT INTO `pid` VALUES (270, 61, 'PRI');
INSERT INTO `pid` VALUES (271, 61, 'PRI');
INSERT INTO `pid` VALUES (272, 62, 'PRI');
INSERT INTO `pid` VALUES (273, 62, 'PRI');
INSERT INTO `pid` VALUES (274, 63, 'PRI');
INSERT INTO `pid` VALUES (275, 64, 'PRI');
INSERT INTO `pid` VALUES (276, 65, 'PRI');
INSERT INTO `pid` VALUES (277, 66, 'PRI');
INSERT INTO `pid` VALUES (278, 67, 'PRI');
INSERT INTO `pid` VALUES (279, 68, 'PRI');
INSERT INTO `pid` VALUES (280, 66, 'PRI');
INSERT INTO `pid` VALUES (281, 67, 'PRI');
INSERT INTO `pid` VALUES (282, 69, 'PRI');
INSERT INTO `pid` VALUES (283, 70, 'PRI');
INSERT INTO `pid` VALUES (284, 66, 'PRI');
INSERT INTO `pid` VALUES (285, 71, 'PRI');
INSERT INTO `pid` VALUES (286, 72, 'PRI');
INSERT INTO `pid` VALUES (287, 69, 'PRI');
INSERT INTO `pid` VALUES (288, 73, 'PRI');
INSERT INTO `pid` VALUES (289, 74, 'PRI');
INSERT INTO `pid` VALUES (290, 69, 'PRI');
INSERT INTO `pid` VALUES (291, 75, 'PRI');
INSERT INTO `pid` VALUES (292, 73, 'PRI');
INSERT INTO `pid` VALUES (293, 76, 'PRI');
INSERT INTO `pid` VALUES (294, 77, 'PRI');
INSERT INTO `pid` VALUES (295, 78, 'PRI');
INSERT INTO `pid` VALUES (296, 76, 'PRI');
INSERT INTO `pid` VALUES (297, 79, 'PRI');
INSERT INTO `pid` VALUES (298, 69, 'PRI');
INSERT INTO `pid` VALUES (299, 76, 'PRI');
INSERT INTO `pid` VALUES (300, 76, 'PRI');
INSERT INTO `pid` VALUES (301, 79, 'PRI');
INSERT INTO `pid` VALUES (302, 70, 'PRI');
INSERT INTO `pid` VALUES (303, 69, 'PRI');
INSERT INTO `pid` VALUES (304, 80, 'PRI');
INSERT INTO `pid` VALUES (305, 81, 'PRI');
INSERT INTO `pid` VALUES (306, 81, 'PRI');
INSERT INTO `pid` VALUES (307, 82, 'PRI');
INSERT INTO `pid` VALUES (308, 69, 'PRI');
INSERT INTO `pid` VALUES (309, 82, 'PRI');
INSERT INTO `pid` VALUES (310, 74, 'PRI');
INSERT INTO `pid` VALUES (311, 75, 'PRI');
INSERT INTO `pid` VALUES (312, 69, 'PRI');
INSERT INTO `pid` VALUES (313, 76, 'PRI');
INSERT INTO `pid` VALUES (314, 79, 'PRI');
INSERT INTO `pid` VALUES (315, 79, 'PRI');
INSERT INTO `pid` VALUES (316, 79, 'PRI');
INSERT INTO `pid` VALUES (317, 79, 'PRI');
INSERT INTO `pid` VALUES (318, 83, 'PRI');
INSERT INTO `pid` VALUES (319, 76, 'PRI');
INSERT INTO `pid` VALUES (320, 82, 'PRI');
INSERT INTO `pid` VALUES (321, 84, 'PRI');
INSERT INTO `pid` VALUES (322, 85, 'PRI');
INSERT INTO `pid` VALUES (323, 86, 'PRI');
INSERT INTO `pid` VALUES (324, 69, 'PRI');
INSERT INTO `pid` VALUES (325, 83, 'PRI');
INSERT INTO `pid` VALUES (326, 82, 'PRI');
INSERT INTO `pid` VALUES (327, 74, 'PRI');
INSERT INTO `pid` VALUES (328, 87, 'PRI');
INSERT INTO `pid` VALUES (329, 74, 'PRI');
INSERT INTO `pid` VALUES (330, 69, 'PRI');
INSERT INTO `pid` VALUES (331, 85, 'PRI');
INSERT INTO `pid` VALUES (332, 82, 'PRI');
INSERT INTO `pid` VALUES (333, 85, 'PRI');
INSERT INTO `pid` VALUES (334, 85, 'PRI');
INSERT INTO `pid` VALUES (335, 88, 'PRI');
INSERT INTO `pid` VALUES (336, 88, 'PRI');
INSERT INTO `pid` VALUES (337, 89, 'PRI');
INSERT INTO `pid` VALUES (338, 89, 'PRI');
INSERT INTO `pid` VALUES (339, 90, 'PRI');
INSERT INTO `pid` VALUES (340, 85, 'PRI');
INSERT INTO `pid` VALUES (341, 76, 'PRI');
INSERT INTO `pid` VALUES (342, 91, 'PRI');
INSERT INTO `pid` VALUES (343, 86, 'PRI');
INSERT INTO `pid` VALUES (344, 90, 'PRI');
INSERT INTO `pid` VALUES (345, 90, 'PRI');
INSERT INTO `pid` VALUES (346, 92, 'PRI');
INSERT INTO `pid` VALUES (347, 91, 'PRI');
INSERT INTO `pid` VALUES (348, 90, 'PRI');
INSERT INTO `pid` VALUES (349, 93, 'WAIT');
INSERT INTO `pid` VALUES (350, 94, 'WAIT');
INSERT INTO `pid` VALUES (351, 95, 'WAIT');
INSERT INTO `pid` VALUES (352, 96, 'WAIT');
INSERT INTO `pid` VALUES (353, 94, 'WAIT');
INSERT INTO `pid` VALUES (354, 97, 'WAIT');
INSERT INTO `pid` VALUES (355, 98, 'WAIT');
INSERT INTO `pid` VALUES (356, 99, 'WAIT');
INSERT INTO `pid` VALUES (357, 100, 'WAIT');
INSERT INTO `pid` VALUES (358, 101, 'WAIT');
INSERT INTO `pid` VALUES (359, 102, 'WAIT');
INSERT INTO `pid` VALUES (360, 103, 'WAIT');
INSERT INTO `pid` VALUES (361, 104, 'WAIT');
INSERT INTO `pid` VALUES (362, 105, 'WAIT');
INSERT INTO `pid` VALUES (363, 105, 'WAIT');
INSERT INTO `pid` VALUES (364, 106, 'WAIT');
INSERT INTO `pid` VALUES (365, 107, 'WAIT');
INSERT INTO `pid` VALUES (366, 107, 'WAIT');
INSERT INTO `pid` VALUES (367, 108, 'WAIT');
INSERT INTO `pid` VALUES (368, 109, 'WAIT');
INSERT INTO `pid` VALUES (369, 109, 'WAIT');
INSERT INTO `pid` VALUES (370, 110, 'WAIT');
INSERT INTO `pid` VALUES (371, 111, 'WAIT');
INSERT INTO `pid` VALUES (372, 112, 'WAIT');
INSERT INTO `pid` VALUES (373, 113, 'WAIT');
INSERT INTO `pid` VALUES (374, 114, 'WAIT');
INSERT INTO `pid` VALUES (375, 115, 'WAIT');
INSERT INTO `pid` VALUES (376, 116, 'WAIT');
INSERT INTO `pid` VALUES (377, 117, 'WAIT');
INSERT INTO `pid` VALUES (378, 118, 'WAIT');
INSERT INTO `pid` VALUES (379, 119, 'WAIT');
INSERT INTO `pid` VALUES (380, 120, 'WAIT');
INSERT INTO `pid` VALUES (381, 121, 'WAIT');
INSERT INTO `pid` VALUES (382, 122, 'WAIT');
INSERT INTO `pid` VALUES (383, 122, 'WAIT');
INSERT INTO `pid` VALUES (384, 123, 'WAIT');
INSERT INTO `pid` VALUES (385, 124, 'WAIT');
INSERT INTO `pid` VALUES (386, 125, 'WAIT');
INSERT INTO `pid` VALUES (387, 126, 'WAIT');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `productoalmacen`
-- 

CREATE TABLE `productoalmacen` (
  `idProductoAlmacen` mediumint(3) NOT NULL auto_increment,
  `idProducto` mediumint(3) NOT NULL,
  `idAlmacen` varchar(3) NOT NULL,
  `paCantidad` varchar(20) NOT NULL,
  PRIMARY KEY  (`idProductoAlmacen`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

-- 
-- Volcar la base de datos para la tabla `productoalmacen`
-- 

INSERT INTO `productoalmacen` VALUES (1, 1, '1', '0.4');
INSERT INTO `productoalmacen` VALUES (2, 1, '2', '3');
INSERT INTO `productoalmacen` VALUES (3, 2, '2', '3');
INSERT INTO `productoalmacen` VALUES (4, 2, '3', '0.2');
INSERT INTO `productoalmacen` VALUES (5, 2, '4', '0.15');
INSERT INTO `productoalmacen` VALUES (6, 0, '', '');
INSERT INTO `productoalmacen` VALUES (7, 0, '', '');
INSERT INTO `productoalmacen` VALUES (8, 0, '', '');
INSERT INTO `productoalmacen` VALUES (9, 0, '', '');
INSERT INTO `productoalmacen` VALUES (10, 0, '', '');
INSERT INTO `productoalmacen` VALUES (11, 0, '', '');
INSERT INTO `productoalmacen` VALUES (12, 0, '', '');
INSERT INTO `productoalmacen` VALUES (13, 0, '', '');
INSERT INTO `productoalmacen` VALUES (14, 0, '', '');
INSERT INTO `productoalmacen` VALUES (15, 0, '', '');
INSERT INTO `productoalmacen` VALUES (16, 0, '', '');
INSERT INTO `productoalmacen` VALUES (17, 0, '', '');
INSERT INTO `productoalmacen` VALUES (18, 0, '', '');
INSERT INTO `productoalmacen` VALUES (19, 0, '', '');
INSERT INTO `productoalmacen` VALUES (20, 0, '', '');
INSERT INTO `productoalmacen` VALUES (21, 0, '', '');
INSERT INTO `productoalmacen` VALUES (22, 0, '', '');
INSERT INTO `productoalmacen` VALUES (23, 0, '', '');
INSERT INTO `productoalmacen` VALUES (24, 0, '', '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `productobase`
-- 

CREATE TABLE `productobase` (
  `idProducto` mediumint(4) NOT NULL auto_increment,
  `pNombre` varchar(500) NOT NULL,
  `pDescripcion` varchar(800) NOT NULL,
  `pPrecio` varchar(20) NOT NULL,
  `pFamilia` varchar(50) NOT NULL,
  `pImg` varchar(50) NOT NULL,
  `pThumb` varchar(50) NOT NULL,
  `idCarta` mediumint(4) NOT NULL,
  PRIMARY KEY  (`idProducto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

-- 
-- Volcar la base de datos para la tabla `productobase`
-- 

INSERT INTO `productobase` VALUES (1, 'PATATAS FRITAS.', 'D_PATATAS FRITAS', '6.00', '3', 'img1.jpg', 'timg1.jpg', 0);
INSERT INTO `productobase` VALUES (2, 'CHAUFA CON LOMO', 'D_CHAUFA CON LOMO', '12', '3', 'img2.jpg', 'timg2.jpg', 1);
INSERT INTO `productobase` VALUES (3, 'CHAUFA', 'D_CHAUFA', '7.00', '3', 'img3.jpg', 'timg3.jpg', 0);
INSERT INTO `productobase` VALUES (4, 'CORVINA EN CREMA DE CHAMPIGNONES Y PORTOBELLO CUBIERTO DE QUESO', 'D_CORVINA EN CREMA DE CHAMPIGNONES Y PORTOBELLO	 ', '13.00', '2', 'img4.jpg', 'timg4.jpg', 0);
INSERT INTO `productobase` VALUES (5, 'SANDWICH DE CHICHARR', 'D_SANDWICH DE CHICHARR', '5.00', '6', 'img5.jpg', 'timg5.jpg', 1);
INSERT INTO `productobase` VALUES (6, 'CLUB SANDWICH ', 'D_CLUB SANDWICH ', '4.50', '1', 'img6.jpg', 'timg6.jpg', 0);
INSERT INTO `productobase` VALUES (7, 'SANDWICH MOZZARELLA ', 'D_SANDWICH MOZZARELLA ', '3.50', '1', 'img7.jpg', 'timg7.jpg', 0);
INSERT INTO `productobase` VALUES (8, 'LITAS ARREBOZADAS', '0', '9.00', '3', 'img8.jpg', 'timg8.jpg', 0);
INSERT INTO `productobase` VALUES (9, 'PAELLA MIXTA', 'D_PATATAS FRITAS', '11.00', '3', 'img9.jpg', 'timg9.jpg', 0);
INSERT INTO `productobase` VALUES (10, 'ARROZ BANDA', 'D_CHAUFA CON LOMO', '12.00', '3', 'img10.jpg', 'timg10.jpg', 1);
INSERT INTO `productobase` VALUES (11, 'ARROZ HORNO', 'D_CHAUFA', '9.00', '3', 'img11.jpg', 'timg11.jpg', 0);
INSERT INTO `productobase` VALUES (12, 'TERNERA', 'D_CORVINA EN C', '14.00', '3', 'img12.jpg', 'timg12.jpg', 1);
INSERT INTO `productobase` VALUES (13, 'LOMO', 'D_SANDWICH DE CHICHARR', '4.50', '3', 'img13.jpg', 'timg5.jpg', 0);
INSERT INTO `productobase` VALUES (14, 'CONEJO AJILLO', 'D_CLUB SANDWICH ', '13.00', '3', 'img14.jpg', 'timg14.jpg', 0);
INSERT INTO `productobase` VALUES (15, 'PIERNA DE CORDERO ', 'D_SANDWICH MOZZARELLA ', '7.00', '3', 'img15.jpg', 'timg7.jpg', 1);
INSERT INTO `productobase` VALUES (16, 'CHULETAS CORDERO', '0', '12.50', '3', 'img16.jpg', 'timg16.jpg', 0);
INSERT INTO `productobase` VALUES (17, 'CEBICHE DE TRUCHA', 'D_CEBICHE DE TRUCHA', '18.00', '10', 'img17.jpg', 'timg17.jpg', 1);
INSERT INTO `productobase` VALUES (19, 'PAPA A LA HUANCAINA', 'D_PAPA A LA HUANCAINA', '10.00', '10', 'img19.jpg', 'timg19.jpg', 1);
INSERT INTO `productobase` VALUES (18, 'TIRADITO DE TRUCHA ', 'D_TIRADITO DE TRUCHA', '18.00', '10', 'img18.jpg', 'timg18.jpg', 1);
INSERT INTO `productobase` VALUES (20, 'PAPA A LA OCOPA', 'D_PAPA A LA OCOPA', '10.00', '10', 'img20.jpg', 'timg20.jpg', 1);
INSERT INTO `productobase` VALUES (21, 'ENSALADA FRESCA', 'D_ENSALADA FRESCA', '10.00', '10', 'img21.jpg', 'timg21.jpg', 0);
INSERT INTO `productobase` VALUES (22, 'ENSALADA DE PALTA', 'D_ENSALADA DE PALTA', '10.00', '10', 'img22.jpg', 'timg22.jpg', 0);
INSERT INTO `productobase` VALUES (23, 'YUQUITAS FRITAS/ SALSA GOLF', 'D_YUQUITAS FRITAS/ SALSA GOLF', '6.00', '10', 'img23.jpg', 'timg23.jpg', 0);
INSERT INTO `productobase` VALUES (24, 'CALDO  DE GALLINA', 'D_CALDO  DE GALLINA', '10.00', '11', 'img24.jpg', 'timg24.jpg', 0);
INSERT INTO `productobase` VALUES (25, 'MONDONGO', 'D_MONDONGO', '10.00', '11', 'img25.jpg', 'timg25.jpg', 1);
INSERT INTO `productobase` VALUES (26, 'CHUPIN DE TRUCHA', 'D_CHUPIN  DE TRUCHA', '15.00', '11', 'img26.jpg', 'timg26.jpg', 0);
INSERT INTO `productobase` VALUES (27, 'DIETA DE POLLO', 'D_DIETA DE POLLO', '10.00', '11', 'img27.jpg', 'timg27.jpg', 0);
INSERT INTO `productobase` VALUES (28, 'SUSTANCIA DE POLLO O CARNE ', 'D_SUSTANCIA DE POLLO O CARNE', '10.00', '11', 'img28.jpg', 'timg28.jpg', 1);
INSERT INTO `productobase` VALUES (29, 'SOPA A LA CRIOLLA', 'D_SOPA A LA CRIOLLA', '12.00', '11', 'img29.jpg', 'timg29.jpg', 1);
INSERT INTO `productobase` VALUES (30, 'PICANTE DE CUY C/ PAPAS Y ARROZ ', 'D_PICANTE DE CUY C/ PAPAS Y ARROZ ', '20.00', '12', 'img30.jpg', 'timg30.jpg', 1);
INSERT INTO `productobase` VALUES (31, 'CUY CHACTADO C/ PAPAS Y ENSALADA', 'D_CUY CHACTADO C/ PAPAS Y ENSALADA', '20.00', '12', 'img31.jpg', 'timg31.jpg', 0);
INSERT INTO `productobase` VALUES (32, 'CUY DORADO', 'D_CUY DORADO', '20.00', '12', 'img32.jpg', 'timg32.jpg', 0);
INSERT INTO `productobase` VALUES (33, 'CONEJO BROASTER C/ YUQUITAS Y ENSALADA', 'D_CONEJO BROASTER C/ YUQUITAS Y ENSALADA', '17.00', '12', 'img33.jpg', 'timg33.jpg', 0);
INSERT INTO `productobase` VALUES (34, 'LOMO FINO C/ PAPA DORADA Y ENSALADA', 'D_LOMO FINO C/ PAPA DORADA Y ENSALADA', '20.00', '12', 'img34.jpg', 'timg34.jpg', 0);
INSERT INTO `productobase` VALUES (35, 'LOMO SALTADO C/ ARROZ', 'D_LOMO SALTADO C/ ARROZ', '22.00', '12', 'img35.jpg', 'timg35.jpg', 0);
INSERT INTO `productobase` VALUES (36, 'TACU TACU C/ LOMO FINO', 'D_TACU TACU C/ LOMO FINO', '22.00', '12', 'img36.jpg', 'timg36.jpg', 0);
INSERT INTO `productobase` VALUES (37, 'TACU TACU C/ LOMO SALTADO', 'D_TACU TACU C/ LOMO SALTADO', '22.00', '12', 'img37.jpg', 'timg37.jpg', 0);
INSERT INTO `productobase` VALUES (38, 'FILETE DE POLLO A LA PARRILLA CON PAPA Y ENSALADA', 'D_FILETE DE POLLO A LA PARRILLA CON PAPA Y ENSALADA', '16.00', '13', 'img38.jpg', 'timg38.jpg', 0);
INSERT INTO `productobase` VALUES (39, 'CHICHARRON DE POLLO CON YUQUITAS Y ENSALADA', 'D_CHICHARRON DE POLLO CON YUQUITAS Y ENSALADA', '20.00', '13', 'img39.jpg', 'timg39.jpg', 0);
INSERT INTO `productobase` VALUES (40, 'POLLO A LA COCA COLA C/ ARROZ', 'D_POLLO A LA COCA COLA C/ ARROZ', '18.00', '13', 'img40.jpg', 'timg40.jpg', 0);
INSERT INTO `productobase` VALUES (41, 'FILETE DE POLLO A LA PARRILLA EN SALSA DE CHAMPI', 'D_FILETE DE POLLO A LA PARRILLA EN SALSA DE CHAMPI', '18.00', '13', 'img41.jpg', 'timg41.jpg', 0);
INSERT INTO `productobase` VALUES (42, 'FILETE DE POLLO A LA PARRILLA EN SALSA DE ESPARRAGOS C/ ARROZ', 'D_FILETE DE POLLO A LA PARRILLA EN SALSA DE ESPARRAGOS C/ ARROZ', '18.00', '13', 'img42.jpg', 'timg42.jpg', 0);
INSERT INTO `productobase` VALUES (43, 'MILANESA DE POLLO CON PAPA Y ENSALADA', 'D_MILANESA DE POLLO CON PAPA Y ENSALADA', '18.00', '13', 'img43.jpg', 'timg43.jpg', 0);
INSERT INTO `productobase` VALUES (44, 'ARROZ CHAUFA DE POLLO', 'D_ARROZ CHAUFA CON POLLO ', '15.00', '13', 'img44.jpg', 'timg44.jpg', 0);
INSERT INTO `productobase` VALUES (45, 'CHULETA A LA PARRILLA CON PAPA Y ENSALADA', 'D_CHULETA A LA PARRILLA CON PAPA Y ENSALADA', '16.00', '14', 'img45.jpg', 'timg45.jpg', 0);
INSERT INTO `productobase` VALUES (46, 'CHICHARRON CON PAPA Y CHOCLO', 'D_CHICHARRON CON PAPA Y CHOCLO', '15.00', '14', 'img46.jpg', 'timg46.jpg', 0);
INSERT INTO `productobase` VALUES (47, 'CHICHARRON COLORADO CON PAPA Y ARROZ ', 'D_CHICHARRON COLORADO CON PAPA Y ARROZ ', '15.00', '14', 'img47.jpg', 'timg47.jpg', 0);
INSERT INTO `productobase` VALUES (48, 'TRUCHA A LA MILANESA CON PAPA Y ENSALADA', 'D_TRUCHA A LA MILANESA CON PAPA Y ENSALADA', '18.00', '15', 'img48.jpg', 'timg48.jpg', 0);
INSERT INTO `productobase` VALUES (49, 'TRUCHA A LA COCA COLA CON ARROZ ', 'D_TRUCHA A LA COCA COLA CON ARROZ', '18.00', '15', 'img49.jpg', 'timg49.jpg', 0);
INSERT INTO `productobase` VALUES (50, 'TRUCHA A LA PLANCHA CON PAPA Y ENSALADA', 'D_TRUCHA A LA PLANCHA CON PAPA Y ENSALADA', '16.00', '15', 'img50.jpg', 'timg50.jpg', 0);
INSERT INTO `productobase` VALUES (51, 'TRUCHA FRITA CON PAPA Y ENSALADA', 'D_TRUCHA FRITA CON PAPA Y ENSALADA', '16.00', '15', 'img51.jpg', 'timg51.jpg', 0);
INSERT INTO `productobase` VALUES (52, 'TRUCHA EN SALSA DE CHAMPI', 'D_TRUCHA EN SALSA DE CHAMPI', '18.00', '15', 'img52.jpg', 'timg52.jpg', 0);
INSERT INTO `productobase` VALUES (53, 'TRUCHA EN SALSA DE ESPARRAGOS C/ ARROZ Y ENSALADA ', 'D_TRUCHA EN SALSA DE ESPARRAGOS C/ ARROZ Y ENSALADA', '18.00', '15', 'img53.jpg', 'timg53.jpg', 0);
INSERT INTO `productobase` VALUES (54, 'TRUCHA AL ', 'D_TRUCHA AL AJO CON ARROZ Y ENSALADA', '18.00', '15', 'img54.jpg', 'timg54.jpg', 0);
INSERT INTO `productobase` VALUES (55, 'CHICHARRON DE TRUCHA CON YUQUITAS Y ENSALADA', 'D_CHICHARRON DE TRUCHA CON YUQUITAS Y ENSALADA', '20.00', '15', 'img55.jpg', 'timg55.jpg', 0);
INSERT INTO `productobase` VALUES (56, 'ARROZ CHAUFA DE TRUCHA', 'D_ARROZ CHAFUA DE TRUCHA', '16.00', '15', 'img56.jpg', 'timg56.jpg', 0);
INSERT INTO `productobase` VALUES (57, 'PACHAMANCA DE 03 SABORES (CUY , CERDO , CORDERO)', 'D_PACHAMANCA DE 03 SABORES (CUY , CERDO , CORDERO)', '30.00', '16', 'img57.jpg', 'timg57.jpg', 0);
INSERT INTO `productobase` VALUES (58, 'PACHAMANCA 02 SABORES (CERDO , CORDERO)', 'D_PACHAMANCA 02 SABORES (CERDO , CORDERO)', '25.00', '16', 'img58.jpg', 'timg58.jpg', 0);
INSERT INTO `productobase` VALUES (59, 'PACHAMANCA 01 SABOR (CORDERO)', 'D_PACHAMANCA 01 SABOR (CORDERO)', '18.00', '16', 'img59.jpg', 'timg59.jpg', 0);
INSERT INTO `productobase` VALUES (60, 'CARNERO AL PALO C/ PAPA Y ENSALADA', 'D_CARNERO AL PALO C/ PAPA Y ENSALADA', '18.00', '16', 'img60.jpg', 'timg60.jpg', 0);
INSERT INTO `productobase` VALUES (61, 'TRIPLE EL CONQUISTADOR (ROCOTO RELLENO , TAMAL , FILETE DE POLLO O CHULETA DE CERDO)', 'D_TRIPLE EL CONQUISTADOR (ROCOTO RELLENO , TAMAL , FILETE DE POLLO O CHULETA DE CERDO)', '25.00', '16', 'img61.jpg', 'timg61.jpg', 0);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `productocarta`
-- 

CREATE TABLE `productocarta` (
  `idProductoCarta` mediumint(4) NOT NULL auto_increment,
  `idCarta` varchar(4) NOT NULL,
  `ipProducto` varchar(4) NOT NULL,
  `pcCosto` varchar(20) NOT NULL,
  `pcDescripcion` varchar(200) NOT NULL,
  `idFamilia` varchar(4) NOT NULL,
  PRIMARY KEY  (`idProductoCarta`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `productocarta`
-- 

INSERT INTO `productocarta` VALUES (1, '1', '10', '20.00', '', '');
INSERT INTO `productocarta` VALUES (2, '1', '18', '18.00', '', '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `productos`
-- 

CREATE TABLE `productos` (
  `idProducto` mediumint(4) NOT NULL auto_increment,
  `pNombre` varchar(500) NOT NULL,
  `pDescripcion` varchar(800) NOT NULL,
  `pPrecio` varchar(20) NOT NULL,
  `pFamilia` varchar(50) NOT NULL,
  `pImg` varchar(50) NOT NULL,
  `pThumb` varchar(50) NOT NULL,
  `idCarta` mediumint(4) NOT NULL,
  `pTicket` varchar(10) NOT NULL,
  PRIMARY KEY  (`idProducto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

-- 
-- Volcar la base de datos para la tabla `productos`
-- 

INSERT INTO `productos` VALUES (1, 'Acevichado Maki', 'Acevichado Maki', '20', '1', 'acevichadomaki.jpg', 'acevichadomaki.jpg', 3, '1');
INSERT INTO `productos` VALUES (2, 'Teriyaki Maki', 'Teriyaki Maki', '20', '1', 'teriyakimaki.jpg', 'teriyakimaki.jpg', 3, '1');
INSERT INTO `productos` VALUES (3, 'Spicy Tuna Roll', 'Spicy Tuna Roll', '20', '1', 'spicytunaroll.jpg', 'spicytunaroll.jpg', 3, '1');
INSERT INTO `productos` VALUES (4, 'Dragon Roll', 'Dragon Roll', '23', '1', 'dragonroll.jpg', 'dragonroll.jpg', 3, '1');
INSERT INTO `productos` VALUES (5, 'Kiro Unagui ', 'Kiro Unagui', '23', '1', 'kirounagui.jpg', 'kirounagui.jpg', 3, '1');
INSERT INTO `productos` VALUES (6, 'Tropical Mango', 'Tropical Mango', '23', '1', 'tropicalmango.jpg', 'tropicalmango.jpg', 3, '1');
INSERT INTO `productos` VALUES (7, 'Avocado Maki', 'Avocado Maki', '23', '1', 'avokadomaki.jpg', 'avokadomaki.jpg', 3, '1');
INSERT INTO `productos` VALUES (8, 'Lomo Maki', 'Lomo Maki', '23', '1', 'lomomaki.jpg', 'lomokai.jpg', 3, '1');
INSERT INTO `productos` VALUES (9, 'Maki Anticuchero', 'Maki Anticuchero', '23', '1', 'makianticuchero.jpg', 'makianticuchero.jpg', 3, '1');
INSERT INTO `productos` VALUES (10, 'Contigo Peru', 'Contigo Peru', '23', '1', 'contigoperu.jpg', 'contigoperu.jpg', 3, '1');
INSERT INTO `productos` VALUES (11, 'Salchipapa', 'Salchipapa clasica', '12', '2', 'salchipapa.jpg', 'salchipapa.jpg', 3, '1');
INSERT INTO `productos` VALUES (12, 'Salchi a lo pobre', 'Salchipapa a lo pobre', '14', '2', 'salchialopobre.jpg', 'salchialopobre.jpg', 3, '1');
INSERT INTO `productos` VALUES (13, 'Salchi burguer', 'salchipapa a lo burguer', '16', '2', 'salchiburguer.jpg', 'salchiburguer.jpg', 3, '1');
INSERT INTO `productos` VALUES (14, 'Alitas bbq', 'Alitas bbq', '15', '2', 'alitasbbq.jpg', 'alitasbbq.jpg', 3, '1');
INSERT INTO `productos` VALUES (15, 'CocaCola ', 'CocaCola', '3', '3', 'cocacola.jpg', 'cocacola.jpg', 3, '1');
INSERT INTO `productos` VALUES (16, 'Inca Cola', 'Inca Cola', '3', '3', 'incacola.jpg', 'incacola.jpg', 3, '1');
INSERT INTO `productos` VALUES (17, 'Sprite', 'Sprite', '3', '3', 'sprite.jpg', 'sprite.jpg', 3, '1');
INSERT INTO `productos` VALUES (18, 'Fanta', 'Fanta', '3', '3', 'fanta.jpg', 'fanta.jpg', 3, '1');
INSERT INTO `productos` VALUES (19, 'Maki Acevichado', 'Maki Acevichado', '20', '1', 'makiacevichado.jpg', 'makiacevichado.jpg', 3, '1');
INSERT INTO `productos` VALUES (20, 'Furai Maki', 'Furai Maki', '20', '1', 'furaimaki.jpg', 'furaimaki.jpg', 3, '1');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `rol`
-- 

CREATE TABLE `rol` (
  `idRol` mediumint(9) NOT NULL auto_increment,
  `rNombre` varchar(50) NOT NULL,
  `rDescripcion` varchar(200) NOT NULL,
  PRIMARY KEY  (`idRol`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- 
-- Volcar la base de datos para la tabla `rol`
-- 

INSERT INTO `rol` VALUES (1, 'Administrador', 'Administrador del Sistema');
INSERT INTO `rol` VALUES (2, 'Mozo', 'Mozo que realiza los pedidos en el local');
INSERT INTO `rol` VALUES (3, 'Cocinero', 'Cocina');
INSERT INTO `rol` VALUES (4, 'Cajero', 'Cajero');
INSERT INTO `rol` VALUES (5, 'Deshabilitado', 'deshabilitado');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `usuario`
-- 

CREATE TABLE `usuario` (
  `idUsuario` mediumint(4) NOT NULL auto_increment,
  `uNombres` varchar(50) NOT NULL,
  `uApellidos` varchar(50) NOT NULL,
  `uRol` varchar(4) NOT NULL,
  `uFecha` datetime NOT NULL,
  `uAlias` varchar(50) NOT NULL,
  `uPass` varchar(50) NOT NULL,
  `uDni` varchar(20) NOT NULL,
  `uFechNacimiento` datetime NOT NULL,
  PRIMARY KEY  (`idUsuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- Volcar la base de datos para la tabla `usuario`
-- 

INSERT INTO `usuario` VALUES (1, 'Sandra', 'Conquistador', '1', '2015-01-31 00:16:03', 'sconquistador', '123456', '', '0000-00-00 00:00:00');
INSERT INTO `usuario` VALUES (2, 'Edgar', 'Gonzales', '1', '2015-01-31 00:16:36', 'egonzales', '123456', '', '0000-00-00 00:00:00');
INSERT INTO `usuario` VALUES (3, 'Henry', 'Casavilca Valer', '1', '0000-00-00 00:00:00', 'hcasavilca', '123456', '', '0000-00-00 00:00:00');
INSERT INTO `usuario` VALUES (4, 'Claudio', 'Tolentino Perez', '1', '2015-01-13 00:00:00', 'ctolentino', '123456', '', '0000-00-00 00:00:00');
INSERT INTO `usuario` VALUES (5, 'Paula', 'Tolentino Perez', '1', '2015-01-13 00:00:00', 'ptolentino', '123456', '45235689', '0000-00-00 00:00:00');
INSERT INTO `usuario` VALUES (6, '', '', '', '0000-00-00 00:00:00', '', '', '', '0000-00-00 00:00:00');
INSERT INTO `usuario` VALUES (7, '', '', '', '0000-00-00 00:00:00', '', '', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `venta`
-- 

CREATE TABLE `venta` (
  `vId` mediumint(5) NOT NULL auto_increment,
  `vCantidadProducto` int(3) NOT NULL,
  `vFecha` datetime NOT NULL,
  `vIdpersona` int(5) NOT NULL,
  `vIdTiket` int(5) NOT NULL,
  `vMesa` int(3) NOT NULL,
  `vEstado` int(4) NOT NULL,
  `vFechaCaja` datetime NOT NULL,
  `vCosto` varchar(20) NOT NULL,
  `vCostoVisa` varchar(20) NOT NULL,
  `vObservacion` varchar(500) NOT NULL,
  `vTimbre` varchar(5) NOT NULL,
  `vEliminado` varchar(5) NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY  (`vId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=131 ;

-- 
-- Volcar la base de datos para la tabla `venta`
-- 

INSERT INTO `venta` VALUES (1, 20, '2014-01-01 00:00:00', 2, 0, 2, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '13:24:00');
INSERT INTO `venta` VALUES (2, 20, '2014-01-01 00:00:00', 3, 0, 2, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '13:26:00');
INSERT INTO `venta` VALUES (3, 20, '2014-01-01 00:00:00', 2, 0, 2, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '13:45:00');
INSERT INTO `venta` VALUES (4, 20, '2014-01-01 00:00:00', 4, 0, 2, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '14:25:00');
INSERT INTO `venta` VALUES (5, 20, '2014-01-01 00:00:00', 3, 0, 1, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '14:49:00');
INSERT INTO `venta` VALUES (6, 20, '2014-01-01 00:00:00', 3, 0, 2, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '16:47:00');
INSERT INTO `venta` VALUES (7, 20, '2014-01-01 00:00:00', 2, 0, 1, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '16:48:00');
INSERT INTO `venta` VALUES (8, 20, '2017-02-14 13:00:18', 2, 0, 26, 0, '2017-02-14 13:29:01', '51', '30', '', '1', '', '13:00:00');
INSERT INTO `venta` VALUES (9, 20, '2017-02-14 13:23:56', 2, 0, 13, 0, '2017-02-14 15:08:14', '', '', '', '1', '', '13:23:00');
INSERT INTO `venta` VALUES (10, 20, '2017-02-14 13:30:49', 3, 0, 14, 0, '2017-02-14 14:23:00', '93', '', '', '1', '', '13:30:00');
INSERT INTO `venta` VALUES (11, 20, '2017-02-14 14:26:33', 2, 0, 21, 0, '2017-02-14 16:28:36', '88', '', '', '1', '', '14:26:00');
INSERT INTO `venta` VALUES (12, 20, '2017-02-14 15:13:50', 3, 0, 24, 0, '2017-02-14 15:34:35', '', '191', '', '1', '', '15:13:00');
INSERT INTO `venta` VALUES (13, 20, '2017-02-14 15:15:42', 2, 0, 22, 0, '2017-02-14 15:17:42', '110', '', '', '1', '', '15:15:00');
INSERT INTO `venta` VALUES (14, 20, '2017-02-14 14:38:27', 2, 0, 26, 0, '2017-02-14 15:37:05', '', '133', '', '1', '', '14:38:00');
INSERT INTO `venta` VALUES (15, 20, '2017-02-14 15:04:55', 3, 0, 19, 0, '2017-02-14 15:52:42', '148', '', '', '1', '', '15:04:00');
INSERT INTO `venta` VALUES (16, 20, '2017-02-14 15:06:29', 3, 0, 17, 0, '2017-02-14 15:48:10', '111', '', '', '1', '', '15:06:00');
INSERT INTO `venta` VALUES (17, 20, '2017-02-14 16:05:04', 2, 0, 13, 0, '2017-02-14 16:28:38', '189', '', '', '1', '', '16:05:00');
INSERT INTO `venta` VALUES (18, 20, '2017-02-14 15:45:26', 2, 0, 14, 0, '2017-02-14 15:49:30', '', '88', '', '1', '', '15:45:00');
INSERT INTO `venta` VALUES (19, 20, '2017-02-14 15:42:53', 3, 0, 20, 0, '2017-02-14 16:28:41', '125', '', '', '1', '', '15:42:00');
INSERT INTO `venta` VALUES (20, 20, '2017-02-14 16:01:22', 2, 0, 15, 0, '2017-02-14 16:28:43', '244', '', '', '1', '', '16:01:00');
INSERT INTO `venta` VALUES (21, 20, '2017-02-14 15:58:05', 2, 0, 22, 0, '2017-02-14 16:28:55', '85', '', '', '1', '', '15:58:00');
INSERT INTO `venta` VALUES (22, 20, '2017-02-15 12:23:26', 3, 0, 4, 0, '2017-02-15 13:12:46', '74', '', '', '1', '', '12:23:00');
INSERT INTO `venta` VALUES (23, 20, '2017-02-15 13:52:44', 2, 0, 15, 0, '2017-02-15 14:11:18', '', '294', '', '1', '', '13:52:00');
INSERT INTO `venta` VALUES (24, 20, '2017-02-15 13:29:44', 3, 0, 13, 0, '2017-02-15 14:27:55', '156', '', '', '1', '', '13:29:00');
INSERT INTO `venta` VALUES (25, 20, '2017-02-15 14:05:02', 2, 0, 22, 0, '2017-02-15 16:08:19', '74', '', '', '1', '', '14:05:00');
INSERT INTO `venta` VALUES (26, 20, '2017-02-15 14:39:16', 3, 0, 26, 0, '2017-02-15 15:15:23', '133', '', '', '1', '', '14:39:00');
INSERT INTO `venta` VALUES (27, 20, '2017-02-15 14:17:27', 2, 0, 21, 0, '2017-02-15 16:08:22', '90', '', '', '1', '', '14:17:00');
INSERT INTO `venta` VALUES (28, 20, '2017-02-15 14:53:14', 3, 0, 14, 0, '2017-02-15 16:08:26', '96', '', '', '1', '', '14:53:00');
INSERT INTO `venta` VALUES (29, 20, '2017-02-16 12:53:04', 2, 0, 22, 0, '2017-02-16 13:56:30', '', '135', '', '1', '', '12:53:00');
INSERT INTO `venta` VALUES (30, 20, '2017-02-16 13:08:54', 3, 0, 14, 0, '2017-02-16 13:41:32', '49', '', '', '1', '', '13:08:00');
INSERT INTO `venta` VALUES (31, 20, '2017-02-16 13:36:38', 2, 0, 13, 0, '2017-02-16 14:20:53', '145', '', '', '1', '', '13:36:00');
INSERT INTO `venta` VALUES (32, 20, '2017-02-16 14:27:18', 3, 0, 21, 0, '2017-02-16 14:37:49', '124', '', '', '1', '', '14:27:00');
INSERT INTO `venta` VALUES (33, 20, '2017-02-16 15:18:20', 2, 0, 27, 0, '2017-02-16 15:23:11', '124', '', '', '1', '', '15:18:00');
INSERT INTO `venta` VALUES (34, 20, '2014-01-01 00:00:00', 3, 0, 1, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '13:54:00');
INSERT INTO `venta` VALUES (35, 20, '2017-02-16 14:22:41', 3, 0, 1, 0, '2017-02-16 15:26:09', '148', '', '', '1', '', '14:22:00');
INSERT INTO `venta` VALUES (36, 20, '2017-02-16 14:41:56', 2, 0, 14, 0, '2017-02-16 15:35:42', '90', '', '', '1', '', '14:41:00');
INSERT INTO `venta` VALUES (37, 20, '2017-02-16 15:40:53', 3, 0, 13, 0, '2017-02-16 15:41:59', '14', '', '', '1', '', '15:40:00');
INSERT INTO `venta` VALUES (38, 20, '2014-01-01 00:00:00', 5, 0, 1, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '12:02:00');
INSERT INTO `venta` VALUES (39, 20, '2017-02-17 12:23:37', 5, 0, 1, 0, '2017-02-17 12:24:23', '2.5', '', '', '1', '', '12:23:00');
INSERT INTO `venta` VALUES (40, 20, '2017-02-17 14:24:44', 3, 0, 4, 0, '2017-02-17 14:36:41', '95', '', '', '1', '', '14:24:00');
INSERT INTO `venta` VALUES (41, 20, '2017-02-17 13:40:48', 2, 0, 21, 0, '2017-02-17 14:39:27', '80', '', '', '1', '', '13:40:00');
INSERT INTO `venta` VALUES (42, 20, '2017-02-17 14:41:23', 2, 0, 4, 0, '2017-02-17 15:13:56', '4', '', '', '1', '', '14:41:00');
INSERT INTO `venta` VALUES (43, 20, '2017-02-17 15:27:58', 3, 0, 22, 0, '2017-02-17 16:27:01', '99', '', '', '1', '', '15:27:00');
INSERT INTO `venta` VALUES (44, 20, '2014-01-01 00:00:00', 4, 0, 1, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '12:14:00');
INSERT INTO `venta` VALUES (45, 20, '2014-01-01 00:00:00', 5, 0, 2, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '12:01:00');
INSERT INTO `venta` VALUES (46, 20, '2014-01-01 00:00:00', 3, 0, 1, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '12:18:00');
INSERT INTO `venta` VALUES (47, 20, '2017-02-18 16:15:22', 5, 0, 14, 0, '2017-02-18 16:19:37', '', '50', '', '1', '', '16:15:00');
INSERT INTO `venta` VALUES (48, 20, '2017-02-18 13:51:51', 2, 0, 27, 0, '2017-02-18 14:21:09', '', '166', '', '1', '', '13:51:00');
INSERT INTO `venta` VALUES (49, 20, '2017-02-18 14:20:03', 3, 0, 21, 0, '2017-02-18 14:59:56', '', '128', '', '1', '', '14:20:00');
INSERT INTO `venta` VALUES (50, 20, '2017-02-18 15:08:26', 2, 0, 17, 0, '2017-02-18 15:24:48', '176', '', '', '1', '', '15:08:00');
INSERT INTO `venta` VALUES (51, 20, '2017-02-18 16:15:09', 3, 0, 30, 0, '2017-02-18 16:29:30', '216', '', '', '1', '', '16:15:00');
INSERT INTO `venta` VALUES (52, 20, '2017-02-18 14:29:56', 2, 0, 22, 0, '2017-02-18 15:10:19', '87', '', '', '1', '', '14:29:00');
INSERT INTO `venta` VALUES (53, 20, '2014-01-01 00:00:00', 2, 0, 2, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '14:44:00');
INSERT INTO `venta` VALUES (54, 20, '2014-01-01 00:00:00', 5, 0, 29, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '14:58:00');
INSERT INTO `venta` VALUES (55, 20, '2017-02-18 16:23:58', 3, 0, 24, 0, '2017-02-18 16:43:01', '203', '', '', '1', '', '16:23:00');
INSERT INTO `venta` VALUES (56, 20, '2017-02-18 15:52:49', 2, 0, 10, 0, '2017-02-18 16:21:23', '5', '', '', '1', '', '15:52:00');
INSERT INTO `venta` VALUES (57, 20, '2014-01-01 00:00:00', 2, 0, 1, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '15:59:00');
INSERT INTO `venta` VALUES (58, 20, '2017-02-18 16:15:07', 2, 0, 21, 0, '2017-02-18 17:06:27', '57', '', '', '1', '', '16:15:00');
INSERT INTO `venta` VALUES (59, 20, '2017-02-18 16:25:57', 3, 0, 14, 0, '2017-02-18 16:26:40', '2.5', '', '', '1', '', '16:25:00');
INSERT INTO `venta` VALUES (60, 20, '2014-01-01 00:00:00', 4, 0, 1, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '11:12:00');
INSERT INTO `venta` VALUES (61, 20, '2014-01-01 00:00:00', 3, 0, 13, 0, '0000-00-00 00:00:00', '', '', 'Mondongo', '1', '2017-', '12:04:00');
INSERT INTO `venta` VALUES (62, 20, '2017-02-19 12:22:09', 3, 0, 13, 0, '2017-02-19 13:50:48', '213', '', '', '1', '', '12:22:00');
INSERT INTO `venta` VALUES (63, 20, '2017-02-19 12:37:05', 4, 0, 21, 0, '2017-02-19 14:21:52', '76', '', '', '1', '', '12:37:00');
INSERT INTO `venta` VALUES (64, 20, '2017-02-19 12:50:48', 3, 0, 2, 0, '2017-02-19 17:53:57', '10', '', '', '1', '', '12:50:00');
INSERT INTO `venta` VALUES (65, 20, '2017-02-19 13:11:44', 4, 0, 4, 0, '2017-02-19 13:50:12', '113', '', '', '1', '', '13:11:00');
INSERT INTO `venta` VALUES (66, 20, '2017-02-19 13:51:58', 5, 0, 27, 0, '2017-02-19 14:30:43', '267', '', '', '1', '', '13:51:00');
INSERT INTO `venta` VALUES (67, 20, '2017-02-19 13:40:28', 4, 0, 9, 0, '2017-02-19 14:16:24', '68', '', '', '1', '', '13:40:00');
INSERT INTO `venta` VALUES (68, 20, '2017-02-19 13:37:27', 4, 0, 12, 0, '2017-02-19 14:26:01', '127', '', 'Plata_primero', '1', '', '13:37:00');
INSERT INTO `venta` VALUES (69, 20, '2017-02-19 16:07:29', 5, 0, 19, 0, '2017-02-19 16:10:05', '330', '', '', '1', '', '16:07:00');
INSERT INTO `venta` VALUES (70, 20, '2017-02-19 14:43:45', 3, 0, 5, 0, '2017-02-19 15:16:40', '276', '', '', '1', '', '14:43:00');
INSERT INTO `venta` VALUES (71, 20, '2017-02-19 14:05:16', 4, 0, 4, 0, '2017-02-19 14:33:43', '', '27', '', '1', '', '14:05:00');
INSERT INTO `venta` VALUES (72, 20, '2017-02-19 14:08:17', 4, 0, 8, 0, '2017-02-19 14:44:47', '', '50', '', '1', '', '14:08:00');
INSERT INTO `venta` VALUES (73, 20, '2017-02-19 14:13:31', 3, 0, 14, 0, '2017-02-19 15:07:07', '', '165', '', '1', '', '14:13:00');
INSERT INTO `venta` VALUES (74, 20, '2017-02-19 16:00:25', 5, 0, 26, 0, '2017-02-19 16:55:02', '195.5', '', '', '1', '', '16:00:00');
INSERT INTO `venta` VALUES (75, 20, '2017-02-19 15:05:22', 4, 0, 10, 0, '2017-02-19 15:25:38', '174', '', 'Mondongos_primero', '1', '', '15:05:00');
INSERT INTO `venta` VALUES (76, 20, '2017-02-19 17:04:12', 3, 0, 11, 0, '2017-02-19 17:05:01', '78', '166', '', '1', '', '17:04:00');
INSERT INTO `venta` VALUES (77, 20, '2017-02-19 14:20:32', 4, 0, 22, 0, '2017-02-19 16:04:24', '15', '100', '', '1', '', '14:20:00');
INSERT INTO `venta` VALUES (78, 20, '2017-02-19 14:21:05', 3, 0, 6, 0, '2017-02-19 15:11:58', '115', '', '', '1', '', '14:21:00');
INSERT INTO `venta` VALUES (79, 20, '2017-02-19 15:28:23', 4, 0, 13, 0, '2017-02-19 15:39:00', '517', '', '', '1', '', '15:28:00');
INSERT INTO `venta` VALUES (80, 20, '2017-02-19 14:49:30', 5, 0, 15, 0, '2017-02-19 16:08:21', '', '126', 'Chicha_tibia', '1', '', '14:49:00');
INSERT INTO `venta` VALUES (81, 20, '2017-02-19 14:52:29', 4, 0, 4, 0, '2017-02-19 15:37:36', '', '72', '', '1', '', '14:52:00');
INSERT INTO `venta` VALUES (82, 20, '2017-02-19 16:10:01', 5, 0, 17, 0, '2017-02-19 16:15:29', '238', '', '', '1', '', '16:10:00');
INSERT INTO `venta` VALUES (83, 20, '2017-02-19 15:49:17', 3, 0, 1, 0, '2017-02-19 17:53:51', '75', '', 'Cuy_solo_sancochado_con_papas_sancochadas', '1', '', '15:49:00');
INSERT INTO `venta` VALUES (84, 20, '2017-02-19 15:40:05', 4, 0, 6, 0, '2017-02-19 16:43:56', '73', '', '', '1', '', '15:40:00');
INSERT INTO `venta` VALUES (85, 20, '2017-02-19 17:02:35', 5, 0, 30, 0, '2017-02-19 17:13:14', '207', '', '', '1', '', '17:02:00');
INSERT INTO `venta` VALUES (86, 20, '2014-01-01 00:00:00', 3, 0, 40, 0, '0000-00-00 00:00:00', '', '', '', '1', '2017-', '17:06:00');
INSERT INTO `venta` VALUES (87, 20, '2017-02-19 15:58:00', 3, 0, 24, 0, '2017-02-19 16:14:17', '5', '', '', '1', '', '15:58:00');
INSERT INTO `venta` VALUES (88, 20, '2017-02-19 16:36:08', 4, 0, 24, 0, '2017-02-19 17:37:31', '44', '', '', '1', '', '16:36:00');
INSERT INTO `venta` VALUES (89, 20, '2017-02-19 16:47:17', 3, 0, 28, 0, '2017-02-19 16:47:51', '4.5', '', '', '1', '', '16:47:00');
INSERT INTO `venta` VALUES (90, 20, '2017-02-19 17:15:52', 3, 0, 21, 0, '2017-02-19 17:47:48', '74', '', '', '1', '', '17:15:00');
INSERT INTO `venta` VALUES (91, 20, '2017-02-19 17:12:08', 3, 0, 22, 0, '2017-02-19 17:53:40', '', '116', '', '1', '', '17:12:00');
INSERT INTO `venta` VALUES (92, 20, '2017-02-19 17:10:56', 4, 0, 39, 0, '2017-02-19 17:12:16', '12.5', '', '3_guaranas_2_san_mateo_sg', '1', '', '17:10:00');
INSERT INTO `venta` VALUES (93, 20, '2019-11-27 16:59:09', 2, 0, 2, 0, '2019-11-27 17:06:22', '59', '', '', '1', '', '16:59:00');
INSERT INTO `venta` VALUES (94, 20, '2019-11-27 17:06:53', 2, 0, 3, 0, '2020-08-12 17:49:02', '114', '', '', '1', '', '17:06:00');
INSERT INTO `venta` VALUES (95, 20, '2019-11-27 17:02:42', 3, 0, 1, 0, '2019-11-27 17:06:11', '12', '12', '', '1', '', '17:02:00');
INSERT INTO `venta` VALUES (96, 20, '2019-11-27 17:04:57', 3, 0, 4, 0, '2020-08-12 17:49:06', '', '40', '', '1', '', '17:04:00');
INSERT INTO `venta` VALUES (97, 20, '2020-07-22 06:32:08', 2, 0, 1, 0, '2021-07-17 16:25:51', '10', '', '', '1', '', '07:32:00');
INSERT INTO `venta` VALUES (98, 20, '2020-07-22 18:25:11', 2, 0, 2, 0, '2020-07-22 18:26:21', '67', '', '', '1', '', '19:25:00');
INSERT INTO `venta` VALUES (99, 20, '2020-08-12 17:47:06', 2, 0, 2, 0, '2020-08-12 17:49:11', '51', '', '', '1', '', '18:47:00');
INSERT INTO `venta` VALUES (100, 20, '2020-08-12 17:47:44', 3, 0, 6, 0, '2020-08-12 17:49:45', '20', '29', '', '1', '', '18:47:00');
INSERT INTO `venta` VALUES (101, 20, '2021-05-19 09:35:25', 3, 0, 3, 0, '2021-07-17 16:25:53', '43', '', '', '1', '', '10:35:00');
INSERT INTO `venta` VALUES (102, 20, '2021-07-06 19:13:43', 2, 0, 2, 0, '2021-07-17 16:25:55', '', '42', '', '1', '', '20:13:00');
INSERT INTO `venta` VALUES (103, 20, '2021-07-17 16:26:07', 3, 0, 2, 0, '2021-07-17 16:28:26', '', '42', '', '1', '', '17:26:00');
INSERT INTO `venta` VALUES (104, 20, '2021-07-17 16:26:22', 3, 0, 12, 0, '2021-07-17 16:28:29', '3', '', '', '1', '', '17:26:00');
INSERT INTO `venta` VALUES (105, 20, '2021-07-17 16:27:50', 3, 0, 1, 0, '2021-07-17 16:28:35', '50', '4', '', '1', '', '17:27:00');
INSERT INTO `venta` VALUES (106, 20, '2014-01-01 00:00:00', 4, 0, 2, 0, '0000-00-00 00:00:00', '', '', '', '1', '2021-', '17:33:00');
INSERT INTO `venta` VALUES (107, 20, '2021-07-17 17:03:43', 3, 0, 1, 0, '2021-07-17 17:04:47', '79', '', '', '1', '', '18:03:00');
INSERT INTO `venta` VALUES (108, 20, '2021-08-28 18:10:48', 3, 0, 3, 0, '2021-08-28 18:12:39', '40', '', '', '1', '', '19:10:00');
INSERT INTO `venta` VALUES (109, 20, '2021-08-28 18:19:55', 3, 0, 2, 0, '2021-09-15 22:17:04', '80', '', '', '1', '', '19:19:00');
INSERT INTO `venta` VALUES (110, 20, '2021-09-04 18:00:57', 3, 0, 11, 0, '2021-09-15 22:17:05', '28', '', '', '1', '', '19:00:00');
INSERT INTO `venta` VALUES (111, 20, '2021-09-15 22:09:15', 3, 0, 1, 0, '2021-09-15 22:18:59', '46', '', '', '1', '', '23:09:00');
INSERT INTO `venta` VALUES (112, 20, '2021-09-15 22:12:06', 4, 0, 3, 0, '2021-09-15 22:19:11', '60', '9', '', '1', '', '23:12:00');
INSERT INTO `venta` VALUES (113, 20, '2021-09-15 22:17:16', 3, 0, 2, 0, '2021-09-15 22:19:03', '', '40', '', '1', '', '23:17:00');
INSERT INTO `venta` VALUES (114, 20, '2021-09-15 22:18:05', 2, 0, 4, 0, '2021-09-15 22:46:21', '40', '', '', '1', '', '23:18:00');
INSERT INTO `venta` VALUES (115, 20, '2021-09-25 16:10:54', 2, 0, 1, 0, '2021-09-25 16:20:05', '20', '', '', '1', '', '17:10:00');
INSERT INTO `venta` VALUES (116, 20, '2021-09-25 16:13:55', 2, 0, 3, 0, '2021-09-25 16:20:06', '', '40', '', '1', '', '17:13:00');
INSERT INTO `venta` VALUES (117, 20, '2021-09-25 16:18:36', 3, 0, 2, 0, '2021-09-25 16:20:08', '43', '', '', '1', '', '17:18:00');
INSERT INTO `venta` VALUES (118, 20, '2014-01-01 00:00:00', 3, 0, 3, 0, '0000-00-00 00:00:00', '', '', '', '1', '2021-', '17:21:00');
INSERT INTO `venta` VALUES (119, 20, '2021-09-25 16:35:25', 2, 0, 1, 0, '2021-10-06 00:02:44', '40', '', '', '1', '', '17:35:00');
INSERT INTO `venta` VALUES (120, 20, '2021-09-25 16:35:37', 2, 0, 2, 0, '2021-10-06 00:02:46', '80', '', '', '1', '', '17:35:00');
INSERT INTO `venta` VALUES (121, 20, '2021-09-25 16:35:44', 2, 0, 3, 0, '2021-10-06 00:02:48', '100', '', '', '1', '', '17:35:00');
INSERT INTO `venta` VALUES (122, 20, '2021-10-05 23:56:36', 5, 0, 5, 0, '2021-10-06 00:06:06', '83', '', 'El_acevichado_con_salsa_picante', '1', '', '00:56:00');
INSERT INTO `venta` VALUES (123, 20, '2021-10-06 00:03:12', 5, 0, 1, 0, '2021-10-06 00:11:13', '40', '', '', '1', '', '01:03:00');
INSERT INTO `venta` VALUES (124, 20, '2021-10-07 00:50:02', 5, 0, 1, 0, '2021-11-02 21:02:04', '20', '', '', '1', '', '01:50:00');
INSERT INTO `venta` VALUES (125, 20, '2014-01-01 00:00:00', 5, 0, 13, 0, '0000-00-00 00:00:00', '', '', '', '1', '2021-', '19:55:00');
INSERT INTO `venta` VALUES (126, 20, '2021-10-23 19:11:15', 5, 0, 14, 0, '0000-00-00 00:00:00', '', '', '', '1', '', '20:11:00');
INSERT INTO `venta` VALUES (127, 20, '2021-10-26 01:31:11', 5, 0, 31, 0, '0000-00-00 00:00:00', '', '', '', '1', '', '02:31:00');
INSERT INTO `venta` VALUES (128, 20, '2021-11-02 20:58:48', 5, 0, 33, 0, '2021-11-02 21:02:06', '20', '', '', '1', '', '21:58:00');
INSERT INTO `venta` VALUES (129, 20, '2021-11-21 11:36:16', 5, 0, 32, 0, '0000-00-00 00:00:00', '', '', '', '1', '', '11:36:00');
INSERT INTO `venta` VALUES (130, 20, '2021-11-21 16:02:31', 5, 0, 33, 0, '0000-00-00 00:00:00', '', '', '', '1', '', '16:02:00');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `venta_detalle`
-- 

CREATE TABLE `venta_detalle` (
  `vdId` mediumint(4) NOT NULL auto_increment,
  `vId` int(9) NOT NULL,
  `vd_idProducto` int(3) NOT NULL,
  `vd_Cantidad` char(5) NOT NULL,
  `vd_Costo` char(5) NOT NULL,
  `vd_Estado` varchar(2) NOT NULL default '0',
  `vd_Eliminado` varchar(20) NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY  (`vdId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=515 ;

-- 
-- Volcar la base de datos para la tabla `venta_detalle`
-- 

INSERT INTO `venta_detalle` VALUES (1, 1, 1, '1', '12', '0', '', '13:24:00');
INSERT INTO `venta_detalle` VALUES (2, 1, 2, '1', '25', '0', '', '13:24:00');
INSERT INTO `venta_detalle` VALUES (3, 2, 11, '1', '25', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (4, 2, 12, '1', '25', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (5, 2, 11, '1', '25', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (6, 2, 12, '1', '25', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (7, 3, 1, '1', '12', '0', '', '13:45:00');
INSERT INTO `venta_detalle` VALUES (8, 3, 2, '1', '25', '0', '', '13:45:00');
INSERT INTO `venta_detalle` VALUES (9, 4, 48, '1', '15', '0', '', '14:25:00');
INSERT INTO `venta_detalle` VALUES (10, 4, 49, '1', '15', '0', '', '14:25:00');
INSERT INTO `venta_detalle` VALUES (11, 4, 50, '1', '15', '0', '', '14:25:00');
INSERT INTO `venta_detalle` VALUES (12, 5, 12, '1', '24', '0', '', '14:49:00');
INSERT INTO `venta_detalle` VALUES (13, 5, 13, '1', '38', '0', '', '14:49:00');
INSERT INTO `venta_detalle` VALUES (14, 6, 4, '1', '26', '0', '', '16:47:00');
INSERT INTO `venta_detalle` VALUES (15, 6, 3, '1', '24', '0', '', '16:47:00');
INSERT INTO `venta_detalle` VALUES (16, 6, 16, '1', '23', '0', '', '16:47:00');
INSERT INTO `venta_detalle` VALUES (17, 7, 5, '8', '120', '0', '', '16:48:00');
INSERT INTO `venta_detalle` VALUES (18, 8, 34, '1', '10', '0', '', '12:59:00');
INSERT INTO `venta_detalle` VALUES (19, 8, 2, '1', '25', '0', '', '13:00:00');
INSERT INTO `venta_detalle` VALUES (20, 8, 16, '2', '46', '0', '', '13:00:00');
INSERT INTO `venta_detalle` VALUES (21, 9, 3, '0', '0', '0', '1', '13:23:00');
INSERT INTO `venta_detalle` VALUES (22, 9, 46, '0', '0', '0', '1', '13:23:00');
INSERT INTO `venta_detalle` VALUES (23, 10, 14, '2', '70', '0', '', '13:30:00');
INSERT INTO `venta_detalle` VALUES (24, 10, 7, '1', '11', '0', '', '13:30:00');
INSERT INTO `venta_detalle` VALUES (25, 10, 23, '1', '12', '0', '', '13:30:00');
INSERT INTO `venta_detalle` VALUES (26, 11, 10, '1', '18', '0', '', '13:46:00');
INSERT INTO `venta_detalle` VALUES (27, 11, 21, '1', '35', '0', '', '13:46:00');
INSERT INTO `venta_detalle` VALUES (28, 11, 18, '1', '30', '0', '', '13:46:00');
INSERT INTO `venta_detalle` VALUES (29, 12, 30, '2', '14', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (30, 13, 17, '1', '32', '0', '', '13:56:00');
INSERT INTO `venta_detalle` VALUES (31, 13, 14, '1', '35', '0', '', '13:56:00');
INSERT INTO `venta_detalle` VALUES (32, 13, 12, '1', '24', '0', '', '13:56:00');
INSERT INTO `venta_detalle` VALUES (33, 13, 23, '1', '12', '0', '', '13:56:00');
INSERT INTO `venta_detalle` VALUES (34, 14, 24, '1', '14', '0', '', '14:03:00');
INSERT INTO `venta_detalle` VALUES (35, 11, 26, '1', '5', '0', '', '14:26:00');
INSERT INTO `venta_detalle` VALUES (36, 14, 14, '2', '70', '0', '', '14:38:00');
INSERT INTO `venta_detalle` VALUES (37, 14, 11, '1', '25', '0', '', '14:38:00');
INSERT INTO `venta_detalle` VALUES (38, 14, 12, '1', '24', '0', '', '14:38:00');
INSERT INTO `venta_detalle` VALUES (39, 12, 11, '2', '50', '0', '', '14:40:00');
INSERT INTO `venta_detalle` VALUES (40, 12, 12, '1', '24', '0', '', '14:40:00');
INSERT INTO `venta_detalle` VALUES (41, 12, 15, '1', '30', '0', '', '14:40:00');
INSERT INTO `venta_detalle` VALUES (42, 12, 19, '1', '38', '0', '', '14:40:00');
INSERT INTO `venta_detalle` VALUES (43, 12, 16, '1', '23', '0', '', '14:40:00');
INSERT INTO `venta_detalle` VALUES (44, 15, 7, '1', '11', '0', '', '15:04:00');
INSERT INTO `venta_detalle` VALUES (45, 15, 12, '3', '72', '0', '', '15:04:00');
INSERT INTO `venta_detalle` VALUES (46, 15, 20, '1', '30', '0', '', '15:04:00');
INSERT INTO `venta_detalle` VALUES (47, 15, 2, '1', '25', '0', '', '15:04:00');
INSERT INTO `venta_detalle` VALUES (48, 15, 36, '1', '10', '0', '', '15:04:00');
INSERT INTO `venta_detalle` VALUES (49, 16, 12, '2', '48', '0', '', '15:06:00');
INSERT INTO `venta_detalle` VALUES (50, 16, 11, '1', '25', '0', '', '15:06:00');
INSERT INTO `venta_detalle` VALUES (51, 16, 3, '1', '24', '0', '', '15:06:00');
INSERT INTO `venta_detalle` VALUES (52, 16, 24, '1', '14', '0', '', '15:06:00');
INSERT INTO `venta_detalle` VALUES (53, 12, 23, '1', '12', '0', '', '15:13:00');
INSERT INTO `venta_detalle` VALUES (54, 13, 29, '1', '7', '0', '', '15:15:00');
INSERT INTO `venta_detalle` VALUES (55, 17, 2, '1', '25', '0', '', '15:22:00');
INSERT INTO `venta_detalle` VALUES (56, 17, 16, '1', '23', '0', '', '15:22:00');
INSERT INTO `venta_detalle` VALUES (57, 17, 8, '1', '20', '0', '', '15:22:00');
INSERT INTO `venta_detalle` VALUES (58, 17, 15, '1', '30', '0', '', '15:22:00');
INSERT INTO `venta_detalle` VALUES (59, 17, 14, '1', '35', '0', '', '15:22:00');
INSERT INTO `venta_detalle` VALUES (60, 17, 24, '1', '14', '0', '', '15:22:00');
INSERT INTO `venta_detalle` VALUES (61, 17, 29, '2', '14', '0', '', '15:22:00');
INSERT INTO `venta_detalle` VALUES (62, 18, 3, '1', '24', '0', '', '15:42:00');
INSERT INTO `venta_detalle` VALUES (63, 18, 8, '1', '20', '0', '', '15:42:00');
INSERT INTO `venta_detalle` VALUES (64, 18, 15, '1', '30', '0', '', '15:42:00');
INSERT INTO `venta_detalle` VALUES (65, 19, 19, '1', '38', '0', '', '15:42:00');
INSERT INTO `venta_detalle` VALUES (66, 19, 13, '1', '38', '0', '', '15:42:00');
INSERT INTO `venta_detalle` VALUES (67, 19, 14, '1', '35', '0', '', '15:42:00');
INSERT INTO `venta_detalle` VALUES (68, 19, 24, '1', '14', '0', '', '15:42:00');
INSERT INTO `venta_detalle` VALUES (69, 18, 24, '1', '14', '0', '', '15:45:00');
INSERT INTO `venta_detalle` VALUES (70, 20, 13, '2', '76', '0', '', '15:54:00');
INSERT INTO `venta_detalle` VALUES (71, 20, 16, '1', '23', '0', '', '15:54:00');
INSERT INTO `venta_detalle` VALUES (72, 20, 8, '1', '20', '0', '', '15:54:00');
INSERT INTO `venta_detalle` VALUES (73, 20, 14, '1', '35', '0', '', '15:54:00');
INSERT INTO `venta_detalle` VALUES (74, 20, 19, '1', '38', '0', '', '15:54:00');
INSERT INTO `venta_detalle` VALUES (75, 21, 21, '1', '35', '0', '', '15:58:00');
INSERT INTO `venta_detalle` VALUES (76, 21, 19, '1', '38', '0', '', '15:58:00');
INSERT INTO `venta_detalle` VALUES (77, 21, 23, '1', '12', '0', '', '15:58:00');
INSERT INTO `venta_detalle` VALUES (78, 20, 24, '1', '14', '0', '', '15:58:00');
INSERT INTO `venta_detalle` VALUES (79, 20, 19, '1', '38', '0', '', '16:01:00');
INSERT INTO `venta_detalle` VALUES (80, 17, 29, '4', '28', '0', '', '16:05:00');
INSERT INTO `venta_detalle` VALUES (81, 22, 15, '1', '30', '0', '', '12:23:00');
INSERT INTO `venta_detalle` VALUES (82, 22, 20, '1', '30', '0', '', '12:23:00');
INSERT INTO `venta_detalle` VALUES (83, 22, 24, '1', '14', '0', '', '12:23:00');
INSERT INTO `venta_detalle` VALUES (84, 23, 22, '1', '38', '0', '', '12:36:00');
INSERT INTO `venta_detalle` VALUES (85, 23, 14, '3', '105', '0', '', '12:36:00');
INSERT INTO `venta_detalle` VALUES (86, 23, 7, '1', '11', '0', '', '12:36:00');
INSERT INTO `venta_detalle` VALUES (87, 23, 4, '0', '0', '0', '1', '12:36:00');
INSERT INTO `venta_detalle` VALUES (88, 23, 23, '1', '12', '0', '', '12:36:00');
INSERT INTO `venta_detalle` VALUES (89, 23, 3, '1', '24', '0', '', '12:43:00');
INSERT INTO `venta_detalle` VALUES (90, 23, 18, '1', '30', '0', '', '13:04:00');
INSERT INTO `venta_detalle` VALUES (91, 23, 13, '1', '38', '0', '', '13:04:00');
INSERT INTO `venta_detalle` VALUES (92, 24, 3, '1', '24', '0', '', '13:12:00');
INSERT INTO `venta_detalle` VALUES (93, 24, 11, '2', '50', '0', '', '13:12:00');
INSERT INTO `venta_detalle` VALUES (94, 24, 14, '1', '35', '0', '', '13:12:00');
INSERT INTO `venta_detalle` VALUES (95, 24, 23, '1', '12', '0', '', '13:12:00');
INSERT INTO `venta_detalle` VALUES (96, 23, 23, '1', '12', '0', '', '13:13:00');
INSERT INTO `venta_detalle` VALUES (97, 24, 14, '1', '35', '0', '', '13:29:00');
INSERT INTO `venta_detalle` VALUES (98, 25, 3, '1', '24', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (99, 25, 13, '1', '38', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (100, 23, 23, '1', '12', '0', '', '13:41:00');
INSERT INTO `venta_detalle` VALUES (101, 23, 23, '1', '12', '0', '', '13:52:00');
INSERT INTO `venta_detalle` VALUES (102, 26, 16, '1', '23', '0', '', '13:56:00');
INSERT INTO `venta_detalle` VALUES (103, 26, 3, '1', '24', '0', '', '13:56:00');
INSERT INTO `venta_detalle` VALUES (104, 26, 22, '1', '38', '0', '', '13:56:00');
INSERT INTO `venta_detalle` VALUES (105, 26, 7, '1', '11', '0', '', '13:56:00');
INSERT INTO `venta_detalle` VALUES (106, 25, 23, '1', '12', '0', '', '14:05:00');
INSERT INTO `venta_detalle` VALUES (107, 27, 16, '1', '23', '0', '', '14:10:00');
INSERT INTO `venta_detalle` VALUES (108, 27, 15, '1', '30', '0', '', '14:10:00');
INSERT INTO `venta_detalle` VALUES (109, 27, 23, '1', '12', '0', '', '14:10:00');
INSERT INTO `venta_detalle` VALUES (110, 27, 11, '1', '25', '0', '', '14:17:00');
INSERT INTO `venta_detalle` VALUES (111, 28, 12, '2', '48', '0', '', '14:21:00');
INSERT INTO `venta_detalle` VALUES (112, 28, 11, '0', '0', '0', '1', '14:21:00');
INSERT INTO `venta_detalle` VALUES (113, 28, 23, '1', '12', '0', '', '14:21:00');
INSERT INTO `venta_detalle` VALUES (114, 28, 12, '1', '24', '0', '', '14:24:00');
INSERT INTO `venta_detalle` VALUES (115, 26, 17, '1', '32', '0', '', '14:25:00');
INSERT INTO `venta_detalle` VALUES (116, 26, 26, '1', '5', '0', '', '14:39:00');
INSERT INTO `venta_detalle` VALUES (117, 28, 23, '1', '12', '0', '', '14:53:00');
INSERT INTO `venta_detalle` VALUES (118, 29, 4, '1', '26', '0', '', '12:08:00');
INSERT INTO `venta_detalle` VALUES (119, 29, 5, '1', '15', '0', '', '12:08:00');
INSERT INTO `venta_detalle` VALUES (120, 29, 14, '0', '0', '0', '1', '12:08:00');
INSERT INTO `venta_detalle` VALUES (121, 29, 28, '1', '2', '0', '', '12:08:00');
INSERT INTO `venta_detalle` VALUES (122, 29, 24, '1', '14', '0', '', '12:08:00');
INSERT INTO `venta_detalle` VALUES (123, 29, 22, '1', '38', '0', '', '12:08:00');
INSERT INTO `venta_detalle` VALUES (124, 29, 22, '1', '38', '0', '', '12:15:00');
INSERT INTO `venta_detalle` VALUES (125, 29, 28, '1', '2', '0', '', '12:53:00');
INSERT INTO `venta_detalle` VALUES (126, 30, 5, '1', '15', '0', '', '13:08:00');
INSERT INTO `venta_detalle` VALUES (127, 30, 8, '1', '20', '0', '', '13:08:00');
INSERT INTO `venta_detalle` VALUES (128, 30, 24, '1', '14', '0', '', '13:08:00');
INSERT INTO `venta_detalle` VALUES (129, 31, 3, '2', '48', '0', '', '13:17:00');
INSERT INTO `venta_detalle` VALUES (130, 31, 16, '1', '23', '0', '', '13:17:00');
INSERT INTO `venta_detalle` VALUES (131, 31, 21, '1', '35', '0', '', '13:17:00');
INSERT INTO `venta_detalle` VALUES (132, 32, 3, '4', '96', '0', '', '13:22:00');
INSERT INTO `venta_detalle` VALUES (133, 32, 24, '1', '14', '0', '', '13:22:00');
INSERT INTO `venta_detalle` VALUES (134, 33, 11, '0', '0', '0', '3', '13:26:00');
INSERT INTO `venta_detalle` VALUES (135, 33, 12, '2', '48', '0', '', '13:30:00');
INSERT INTO `venta_detalle` VALUES (136, 33, 16, '1', '23', '0', '', '13:30:00');
INSERT INTO `venta_detalle` VALUES (137, 33, 30, '1', '7', '0', '', '13:30:00');
INSERT INTO `venta_detalle` VALUES (138, 31, 2, '1', '25', '0', '', '13:31:00');
INSERT INTO `venta_detalle` VALUES (139, 31, 31, '2', '14', '0', '', '13:36:00');
INSERT INTO `venta_detalle` VALUES (140, 34, 23, '1', '12', '0', '', '13:54:00');
INSERT INTO `venta_detalle` VALUES (141, 33, 17, '1', '32', '0', '', '14:14:00');
INSERT INTO `venta_detalle` VALUES (142, 33, 30, '1', '7', '0', '', '14:14:00');
INSERT INTO `venta_detalle` VALUES (143, 33, 17, '0', '0', '0', '1', '14:21:00');
INSERT INTO `venta_detalle` VALUES (144, 35, 14, '1', '35', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (145, 35, 17, '1', '32', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (146, 35, 8, '1', '20', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (147, 35, 4, '1', '26', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (148, 35, 26, '1', '5', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (149, 35, 15, '1', '30', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (150, 33, 17, '0', '0', '1', '1', '14:23:00');
INSERT INTO `venta_detalle` VALUES (151, 32, 24, '1', '14', '0', '', '14:27:00');
INSERT INTO `venta_detalle` VALUES (152, 36, 13, '2', '76', '0', '', '14:41:00');
INSERT INTO `venta_detalle` VALUES (153, 36, 24, '1', '14', '0', '', '14:41:00');
INSERT INTO `venta_detalle` VALUES (154, 33, 30, '1', '7', '0', '', '14:51:00');
INSERT INTO `venta_detalle` VALUES (155, 33, 23, '0', '0', '0', '1', '15:18:00');
INSERT INTO `venta_detalle` VALUES (156, 37, 23, '0', '0', '0', '1', '15:40:00');
INSERT INTO `venta_detalle` VALUES (157, 37, 24, '1', '14', '0', '', '15:40:00');
INSERT INTO `venta_detalle` VALUES (158, 38, 23, '1', '12', '0', '', '12:02:00');
INSERT INTO `venta_detalle` VALUES (159, 38, 1, '1', '10', '0', '', '12:02:00');
INSERT INTO `venta_detalle` VALUES (160, 39, 64, '1', '2.5', '0', '', '12:23:00');
INSERT INTO `venta_detalle` VALUES (161, 40, 14, '1', '35', '0', '', '13:27:00');
INSERT INTO `venta_detalle` VALUES (162, 40, 11, '1', '25', '0', '', '13:27:00');
INSERT INTO `venta_detalle` VALUES (163, 41, 38, '2', '10', '0', '', '13:40:00');
INSERT INTO `venta_detalle` VALUES (164, 41, 14, '2', '70', '0', '', '13:40:00');
INSERT INTO `venta_detalle` VALUES (165, 40, 23, '1', '12', '0', '', '14:18:00');
INSERT INTO `venta_detalle` VALUES (166, 40, 16, '1', '23', '0', '', '14:24:00');
INSERT INTO `venta_detalle` VALUES (167, 42, 28, '2', '4', '0', '', '14:41:00');
INSERT INTO `venta_detalle` VALUES (168, 43, 12, '1', '24', '0', '', '15:11:00');
INSERT INTO `venta_detalle` VALUES (169, 43, 13, '1', '38', '0', '', '15:11:00');
INSERT INTO `venta_detalle` VALUES (170, 43, 8, '1', '20', '0', '', '15:11:00');
INSERT INTO `venta_detalle` VALUES (171, 43, 52, '1', '15', '0', '', '15:27:00');
INSERT INTO `venta_detalle` VALUES (172, 43, 28, '1', '2', '0', '', '15:27:00');
INSERT INTO `venta_detalle` VALUES (173, 44, 1, '0', '0', '0', '1', '11:50:00');
INSERT INTO `venta_detalle` VALUES (174, 44, 23, '0', '0', '0', '1', '11:50:00');
INSERT INTO `venta_detalle` VALUES (175, 44, 2, '0', '0', '0', '1', '11:53:00');
INSERT INTO `venta_detalle` VALUES (176, 44, 12, '0', '0', '0', '1', '11:54:00');
INSERT INTO `venta_detalle` VALUES (177, 45, 11, '0', '0', '0', '1', '11:59:00');
INSERT INTO `venta_detalle` VALUES (178, 45, 11, '0', '0', '0', '1', '12:01:00');
INSERT INTO `venta_detalle` VALUES (179, 44, 4, '1', '26', '0', '', '12:14:00');
INSERT INTO `venta_detalle` VALUES (180, 46, 12, '1', '24', '0', '', '12:18:00');
INSERT INTO `venta_detalle` VALUES (181, 46, 33, '1', '8', '0', '', '12:18:00');
INSERT INTO `venta_detalle` VALUES (182, 47, 48, '1', '15', '0', '', '12:50:00');
INSERT INTO `venta_detalle` VALUES (183, 47, 14, '1', '35', '0', '', '12:50:00');
INSERT INTO `venta_detalle` VALUES (184, 47, 43, '0', '0', '0', '1', '13:03:00');
INSERT INTO `venta_detalle` VALUES (185, 48, 2, '1', '25', '0', '', '13:20:00');
INSERT INTO `venta_detalle` VALUES (186, 48, 11, '1', '25', '0', '', '13:20:00');
INSERT INTO `venta_detalle` VALUES (187, 48, 12, '2', '48', '0', '', '13:20:00');
INSERT INTO `venta_detalle` VALUES (188, 48, 8, '2', '40', '0', '', '13:20:00');
INSERT INTO `venta_detalle` VALUES (189, 48, 24, '1', '14', '0', '', '13:20:00');
INSERT INTO `venta_detalle` VALUES (190, 49, 3, '1', '24', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (191, 49, 15, '1', '30', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (192, 49, 14, '1', '35', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (193, 49, 24, '1', '14', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (194, 48, 24, '1', '14', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (195, 49, 49, '1', '15', '0', '', '14:20:00');
INSERT INTO `venta_detalle` VALUES (196, 49, 59, '1', '10', '0', '', '14:20:00');
INSERT INTO `venta_detalle` VALUES (197, 50, 11, '2', '50', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (198, 50, 14, '2', '70', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (199, 50, 34, '1', '10', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (200, 50, 16, '1', '23', '0', '', '14:23:00');
INSERT INTO `venta_detalle` VALUES (201, 51, 11, '1', '25', '0', '', '14:25:00');
INSERT INTO `venta_detalle` VALUES (202, 51, 1, '1', '10', '0', '', '14:25:00');
INSERT INTO `venta_detalle` VALUES (203, 51, 10, '1', '18', '0', '', '14:25:00');
INSERT INTO `venta_detalle` VALUES (204, 52, 33, '1', '8', '0', '', '14:27:00');
INSERT INTO `venta_detalle` VALUES (205, 52, 26, '1', '5', '0', '', '14:27:00');
INSERT INTO `venta_detalle` VALUES (206, 52, 11, '1', '25', '0', '', '14:29:00');
INSERT INTO `venta_detalle` VALUES (207, 52, 3, '1', '24', '0', '', '14:29:00');
INSERT INTO `venta_detalle` VALUES (208, 52, 2, '1', '25', '0', '', '14:29:00');
INSERT INTO `venta_detalle` VALUES (209, 51, 5, '1', '15', '0', '', '14:34:00');
INSERT INTO `venta_detalle` VALUES (210, 53, 14, '1', '35', '0', '', '14:44:00');
INSERT INTO `venta_detalle` VALUES (211, 51, 15, '1', '30', '0', '', '14:53:00');
INSERT INTO `venta_detalle` VALUES (212, 51, 51, '2', '30', '0', '', '14:55:00');
INSERT INTO `venta_detalle` VALUES (213, 54, 3, '1', '24', '0', '', '14:58:00');
INSERT INTO `venta_detalle` VALUES (214, 54, 8, '1', '20', '0', '', '14:58:00');
INSERT INTO `venta_detalle` VALUES (215, 51, 8, '1', '20', '0', '', '15:01:00');
INSERT INTO `venta_detalle` VALUES (216, 50, 16, '1', '23', '0', '', '15:08:00');
INSERT INTO `venta_detalle` VALUES (217, 55, 7, '1', '11', '0', '', '15:09:00');
INSERT INTO `venta_detalle` VALUES (218, 55, 3, '1', '24', '0', '', '15:14:00');
INSERT INTO `venta_detalle` VALUES (219, 55, 14, '1', '35', '0', '', '15:14:00');
INSERT INTO `venta_detalle` VALUES (220, 55, 11, '1', '25', '0', '', '15:14:00');
INSERT INTO `venta_detalle` VALUES (221, 51, 46, '1', '20', '0', '', '15:16:00');
INSERT INTO `venta_detalle` VALUES (222, 51, 55, '1', '18', '0', '', '15:16:00');
INSERT INTO `venta_detalle` VALUES (223, 51, 57, '1', '10', '0', '', '15:16:00');
INSERT INTO `venta_detalle` VALUES (224, 55, 32, '1', '8', '0', '', '15:33:00');
INSERT INTO `venta_detalle` VALUES (225, 55, 23, '1', '12', '0', '', '15:33:00');
INSERT INTO `venta_detalle` VALUES (226, 55, 13, '1', '38', '0', '', '15:37:00');
INSERT INTO `venta_detalle` VALUES (227, 55, 8, '1', '20', '0', '', '15:48:00');
INSERT INTO `venta_detalle` VALUES (228, 56, 27, '1', '5', '0', '', '15:52:00');
INSERT INTO `venta_detalle` VALUES (229, 57, 43, '1', '20', '0', '', '15:59:00');
INSERT INTO `venta_detalle` VALUES (230, 57, 48, '1', '15', '0', '', '15:59:00');
INSERT INTO `venta_detalle` VALUES (231, 55, 51, '1', '15', '0', '', '16:03:00');
INSERT INTO `venta_detalle` VALUES (232, 58, 11, '1', '25', '0', '', '16:15:00');
INSERT INTO `venta_detalle` VALUES (233, 58, 8, '1', '20', '0', '', '16:15:00');
INSERT INTO `venta_detalle` VALUES (234, 58, 23, '1', '12', '0', '', '16:15:00');
INSERT INTO `venta_detalle` VALUES (235, 51, 43, '1', '20', '0', '', '16:15:00');
INSERT INTO `venta_detalle` VALUES (236, 47, 60, '0', '0', '0', '1', '16:15:00');
INSERT INTO `venta_detalle` VALUES (237, 55, 51, '1', '15', '0', '', '16:23:00');
INSERT INTO `venta_detalle` VALUES (238, 59, 64, '1', '2.5', '0', '', '16:25:00');
INSERT INTO `venta_detalle` VALUES (239, 60, 11, '1', '25', '0', '', '11:12:00');
INSERT INTO `venta_detalle` VALUES (240, 60, 23, '1', '12', '0', '', '11:12:00');
INSERT INTO `venta_detalle` VALUES (241, 61, 69, '1', '25', '0', '', '12:03:00');
INSERT INTO `venta_detalle` VALUES (242, 61, 24, '1', '14', '0', '', '12:04:00');
INSERT INTO `venta_detalle` VALUES (243, 62, 69, '1', '12', '0', '', '12:11:00');
INSERT INTO `venta_detalle` VALUES (244, 62, 24, '1', '14', '0', '', '12:11:00');
INSERT INTO `venta_detalle` VALUES (245, 62, 69, '3', '36', '0', '', '12:22:00');
INSERT INTO `venta_detalle` VALUES (246, 62, 15, '2', '60', '0', '', '12:22:00');
INSERT INTO `venta_detalle` VALUES (247, 62, 12, '1', '24', '0', '', '12:22:00');
INSERT INTO `venta_detalle` VALUES (248, 62, 2, '1', '25', '0', '', '12:22:00');
INSERT INTO `venta_detalle` VALUES (249, 62, 10, '1', '18', '0', '', '12:22:00');
INSERT INTO `venta_detalle` VALUES (250, 62, 24, '1', '14', '0', '', '12:22:00');
INSERT INTO `venta_detalle` VALUES (251, 62, 36, '1', '10', '0', '', '12:22:00');
INSERT INTO `venta_detalle` VALUES (252, 63, 7, '1', '11', '0', '', '12:37:00');
INSERT INTO `venta_detalle` VALUES (253, 63, 15, '1', '30', '0', '', '12:37:00');
INSERT INTO `venta_detalle` VALUES (254, 63, 69, '1', '12', '0', '', '12:37:00');
INSERT INTO `venta_detalle` VALUES (255, 63, 33, '1', '8', '0', '', '12:37:00');
INSERT INTO `venta_detalle` VALUES (256, 63, 52, '1', '15', '0', '', '12:37:00');
INSERT INTO `venta_detalle` VALUES (257, 64, 36, '1', '10', '0', '', '12:50:00');
INSERT INTO `venta_detalle` VALUES (258, 65, 3, '1', '24', '0', '', '13:11:00');
INSERT INTO `venta_detalle` VALUES (259, 65, 7, '1', '11', '0', '', '13:11:00');
INSERT INTO `venta_detalle` VALUES (260, 65, 13, '1', '38', '0', '', '13:11:00');
INSERT INTO `venta_detalle` VALUES (261, 65, 15, '1', '30', '0', '', '13:11:00');
INSERT INTO `venta_detalle` VALUES (262, 65, 25, '1', '10', '0', '', '13:11:00');
INSERT INTO `venta_detalle` VALUES (263, 66, 13, '1', '38', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (264, 66, 14, '1', '35', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (265, 66, 4, '1', '26', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (266, 66, 3, '1', '24', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (267, 66, 2, '1', '25', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (268, 66, 69, '1', '12', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (269, 66, 17, '1', '32', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (270, 66, 16, '1', '23', '0', '', '13:26:00');
INSERT INTO `venta_detalle` VALUES (271, 67, 23, '1', '12', '0', '', '13:33:00');
INSERT INTO `venta_detalle` VALUES (272, 68, 16, '1', '23', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (273, 68, 11, '1', '25', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (274, 68, 12, '1', '24', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (275, 68, 18, '1', '30', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (276, 68, 5, '1', '15', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (277, 68, 35, '1', '10', '0', '', '13:37:00');
INSERT INTO `venta_detalle` VALUES (278, 66, 24, '2', '28', '0', '', '13:39:00');
INSERT INTO `venta_detalle` VALUES (279, 67, 15, '1', '30', '0', '', '13:40:00');
INSERT INTO `venta_detalle` VALUES (280, 67, 3, '1', '24', '0', '', '13:40:00');
INSERT INTO `venta_detalle` VALUES (281, 67, 28, '1', '2', '0', '', '13:40:00');
INSERT INTO `venta_detalle` VALUES (282, 69, 32, '1', '8', '0', '', '13:48:00');
INSERT INTO `venta_detalle` VALUES (283, 70, 15, '3', '90', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (284, 70, 11, '2', '50', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (285, 70, 16, '2', '46', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (286, 70, 69, '2', '24', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (287, 70, 12, '1', '24', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (288, 70, 10, '1', '18', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (289, 70, 61, '1', '5', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (290, 70, 24, '1', '14', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (291, 66, 69, '2', '24', '0', '', '13:51:00');
INSERT INTO `venta_detalle` VALUES (292, 71, 11, '1', '25', '0', '', '14:05:00');
INSERT INTO `venta_detalle` VALUES (293, 71, 28, '1', '2', '0', '', '14:05:00');
INSERT INTO `venta_detalle` VALUES (294, 72, 13, '1', '38', '0', '', '14:08:00');
INSERT INTO `venta_detalle` VALUES (295, 72, 23, '1', '12', '0', '', '14:08:00');
INSERT INTO `venta_detalle` VALUES (296, 69, 14, '1', '35', '0', '', '14:09:00');
INSERT INTO `venta_detalle` VALUES (297, 69, 69, '1', '12', '0', '', '14:09:00');
INSERT INTO `venta_detalle` VALUES (298, 69, 24, '1', '14', '0', '', '14:09:00');
INSERT INTO `venta_detalle` VALUES (299, 69, 56, '1', '18', '0', '', '14:09:00');
INSERT INTO `venta_detalle` VALUES (300, 69, 16, '1', '23', '0', '', '14:09:00');
INSERT INTO `venta_detalle` VALUES (301, 69, 3, '1', '24', '0', '', '14:09:00');
INSERT INTO `venta_detalle` VALUES (302, 73, 52, '1', '15', '0', '', '14:10:00');
INSERT INTO `venta_detalle` VALUES (303, 73, 50, '1', '15', '0', '', '14:10:00');
INSERT INTO `venta_detalle` VALUES (304, 74, 69, '2', '24', '0', '', '14:11:00');
INSERT INTO `venta_detalle` VALUES (305, 74, 13, '1', '38', '0', '', '14:11:00');
INSERT INTO `venta_detalle` VALUES (306, 74, 7, '2', '22', '0', '', '14:11:00');
INSERT INTO `venta_detalle` VALUES (307, 74, 2, '1', '25', '0', '', '14:11:00');
INSERT INTO `venta_detalle` VALUES (308, 74, 3, '1', '24', '0', '', '14:11:00');
INSERT INTO `venta_detalle` VALUES (309, 74, 23, '1', '12', '0', '', '14:11:00');
INSERT INTO `venta_detalle` VALUES (310, 69, 22, '1', '38', '0', '', '14:12:00');
INSERT INTO `venta_detalle` VALUES (311, 69, 13, '1', '38', '0', '', '14:12:00');
INSERT INTO `venta_detalle` VALUES (312, 75, 14, '1', '35', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (313, 75, 69, '4', '48', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (314, 75, 16, '1', '23', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (315, 75, 12, '2', '48', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (316, 75, 25, '1', '10', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (317, 73, 2, '1', '25', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (318, 73, 14, '2', '70', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (319, 73, 18, '1', '30', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (320, 73, 25, '1', '10', '0', '', '14:13:00');
INSERT INTO `venta_detalle` VALUES (321, 76, 28, '1', '2', '0', '', '14:15:00');
INSERT INTO `venta_detalle` VALUES (322, 77, 12, '1', '24', '0', '', '14:20:00');
INSERT INTO `venta_detalle` VALUES (323, 77, 11, '1', '25', '0', '', '14:20:00');
INSERT INTO `venta_detalle` VALUES (324, 77, 14, '1', '35', '0', '', '14:20:00');
INSERT INTO `venta_detalle` VALUES (325, 77, 25, '1', '10', '0', '', '14:20:00');
INSERT INTO `venta_detalle` VALUES (326, 77, 32, '2', '16', '0', '', '14:20:00');
INSERT INTO `venta_detalle` VALUES (327, 77, 26, '1', '5', '0', '', '14:20:00');
INSERT INTO `venta_detalle` VALUES (328, 78, 7, '1', '11', '0', '', '14:21:00');
INSERT INTO `venta_detalle` VALUES (329, 78, 16, '1', '23', '0', '', '14:21:00');
INSERT INTO `venta_detalle` VALUES (330, 78, 19, '2', '76', '0', '', '14:21:00');
INSERT INTO `venta_detalle` VALUES (331, 78, 26, '1', '5', '0', '', '14:21:00');
INSERT INTO `venta_detalle` VALUES (332, 76, 17, '1', '32', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (333, 76, 12, '1', '24', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (334, 79, 11, '2', '50', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (335, 79, 14, '2', '70', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (336, 79, 13, '1', '38', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (337, 79, 4, '1', '26', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (338, 79, 15, '4', '120', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (339, 79, 12, '1', '24', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (340, 79, 8, '1', '20', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (341, 79, 7, '2', '22', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (342, 79, 24, '2', '28', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (343, 79, 25, '2', '20', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (344, 79, 36, '1', '10', '0', '', '14:22:00');
INSERT INTO `venta_detalle` VALUES (345, 69, 16, '1', '23', '0', '', '14:23:00');
INSERT INTO `venta_detalle` VALUES (346, 69, 49, '1', '15', '0', '', '14:23:00');
INSERT INTO `venta_detalle` VALUES (347, 76, 69, '2', '24', '0', '1', '14:27:00');
INSERT INTO `venta_detalle` VALUES (348, 76, 4, '1', '26', '0', '', '14:27:00');
INSERT INTO `venta_detalle` VALUES (349, 76, 12, '1', '24', '0', '', '14:27:00');
INSERT INTO `venta_detalle` VALUES (350, 76, 2, '1', '25', '0', '', '14:27:00');
INSERT INTO `venta_detalle` VALUES (351, 76, 7, '2', '22', '0', '', '14:27:00');
INSERT INTO `venta_detalle` VALUES (352, 76, 16, '1', '23', '0', '', '14:35:00');
INSERT INTO `venta_detalle` VALUES (353, 76, 18, '1', '30', '0', '', '14:35:00');
INSERT INTO `venta_detalle` VALUES (354, 79, 61, '1', '5', '0', '', '14:39:00');
INSERT INTO `venta_detalle` VALUES (355, 70, 26, '1', '5', '0', '', '14:43:00');
INSERT INTO `venta_detalle` VALUES (356, 69, 2, '0', '0', '0', '1', '14:44:00');
INSERT INTO `venta_detalle` VALUES (357, 69, 24, '1', '14', '0', '', '14:44:00');
INSERT INTO `venta_detalle` VALUES (358, 80, 3, '1', '24', '0', '', '14:49:00');
INSERT INTO `venta_detalle` VALUES (359, 80, 15, '2', '60', '0', '', '14:49:00');
INSERT INTO `venta_detalle` VALUES (360, 80, 17, '1', '32', '0', '', '14:49:00');
INSERT INTO `venta_detalle` VALUES (361, 80, 25, '1', '10', '0', '', '14:49:00');
INSERT INTO `venta_detalle` VALUES (362, 81, 13, '1', '38', '0', '', '14:51:00');
INSERT INTO `venta_detalle` VALUES (363, 81, 12, '1', '24', '0', '', '14:51:00');
INSERT INTO `venta_detalle` VALUES (364, 81, 24, '0', '0', '0', '1', '14:51:00');
INSERT INTO `venta_detalle` VALUES (365, 81, 25, '1', '10', '0', '', '14:52:00');
INSERT INTO `venta_detalle` VALUES (366, 82, 12, '1', '24', '0', '', '14:54:00');
INSERT INTO `venta_detalle` VALUES (367, 82, 19, '2', '76', '0', '', '14:54:00');
INSERT INTO `venta_detalle` VALUES (368, 82, 3, '1', '24', '0', '', '14:54:00');
INSERT INTO `venta_detalle` VALUES (369, 82, 17, '1', '32', '0', '', '14:54:00');
INSERT INTO `venta_detalle` VALUES (370, 82, 25, '1', '10', '0', '', '14:54:00');
INSERT INTO `venta_detalle` VALUES (371, 82, 52, '1', '15', '0', '', '14:54:00');
INSERT INTO `venta_detalle` VALUES (372, 69, 49, '2', '30', '0', '', '14:56:00');
INSERT INTO `venta_detalle` VALUES (373, 82, 20, '1', '30', '0', '', '14:59:00');
INSERT INTO `venta_detalle` VALUES (374, 74, 23, '1', '12', '0', '', '15:03:00');
INSERT INTO `venta_detalle` VALUES (375, 74, 64, '1', '2.5', '0', '', '15:03:00');
INSERT INTO `venta_detalle` VALUES (376, 75, 25, '1', '10', '0', '', '15:05:00');
INSERT INTO `venta_detalle` VALUES (377, 69, 32, '1', '8', '0', '', '15:11:00');
INSERT INTO `venta_detalle` VALUES (378, 76, 36, '1', '10', '0', '', '15:17:00');
INSERT INTO `venta_detalle` VALUES (379, 79, 24, '1', '14', '0', '', '15:18:00');
INSERT INTO `venta_detalle` VALUES (380, 79, 39, '0', '0', '0', '1', '15:18:00');
INSERT INTO `venta_detalle` VALUES (381, 79, 36, '1', '10', '0', '', '15:23:00');
INSERT INTO `venta_detalle` VALUES (382, 79, 44, '0', '0', '0', '2', '15:24:00');
INSERT INTO `venta_detalle` VALUES (383, 79, 43, '1', '20', '0', '', '15:24:00');
INSERT INTO `venta_detalle` VALUES (384, 79, 46, '2', '40', '0', '', '15:28:00');
INSERT INTO `venta_detalle` VALUES (385, 83, 15, '1', '30', '0', '', '15:30:00');
INSERT INTO `venta_detalle` VALUES (386, 83, 14, '1', '35', '0', '', '15:30:00');
INSERT INTO `venta_detalle` VALUES (387, 76, 28, '1', '2', '0', '', '15:35:00');
INSERT INTO `venta_detalle` VALUES (388, 82, 28, '1', '2', '0', '', '15:39:00');
INSERT INTO `venta_detalle` VALUES (389, 84, 15, '1', '30', '0', '', '15:40:00');
INSERT INTO `venta_detalle` VALUES (390, 84, 13, '1', '38', '0', '', '15:40:00');
INSERT INTO `venta_detalle` VALUES (391, 84, 26, '1', '5', '0', '', '15:40:00');
INSERT INTO `venta_detalle` VALUES (392, 85, 15, '1', '30', '0', '', '15:43:00');
INSERT INTO `venta_detalle` VALUES (393, 85, 12, '2', '48', '0', '', '15:43:00');
INSERT INTO `venta_detalle` VALUES (394, 85, 16, '1', '23', '0', '', '15:43:00');
INSERT INTO `venta_detalle` VALUES (395, 85, 11, '1', '25', '0', '', '15:43:00');
INSERT INTO `venta_detalle` VALUES (396, 85, 24, '1', '14', '0', '', '15:43:00');
INSERT INTO `venta_detalle` VALUES (397, 85, 31, '1', '7', '0', '', '15:43:00');
INSERT INTO `venta_detalle` VALUES (398, 86, 18, '1', '30', '0', '', '15:45:00');
INSERT INTO `venta_detalle` VALUES (399, 86, 13, '1', '38', '0', '', '15:45:00');
INSERT INTO `venta_detalle` VALUES (400, 86, 14, '1', '35', '0', '', '15:45:00');
INSERT INTO `venta_detalle` VALUES (401, 86, 11, '1', '25', '0', '', '15:45:00');
INSERT INTO `venta_detalle` VALUES (402, 69, 52, '2', '30', '0', '', '15:48:00');
INSERT INTO `venta_detalle` VALUES (403, 83, 59, '1', '10', '0', '', '15:49:00');
INSERT INTO `venta_detalle` VALUES (404, 82, 61, '1', '5', '0', '', '15:53:00');
INSERT INTO `venta_detalle` VALUES (405, 74, 53, '1', '18', '0', '', '15:56:00');
INSERT INTO `venta_detalle` VALUES (406, 87, 26, '1', '5', '0', '', '15:58:00');
INSERT INTO `venta_detalle` VALUES (407, 74, 53, '1', '18', '0', '', '16:00:00');
INSERT INTO `venta_detalle` VALUES (408, 69, 61, '0', '0', '0', '1', '16:07:00');
INSERT INTO `venta_detalle` VALUES (409, 85, 56, '1', '18', '0', '', '16:08:00');
INSERT INTO `venta_detalle` VALUES (410, 85, 31, '1', '7', '0', '', '16:08:00');
INSERT INTO `venta_detalle` VALUES (411, 85, 51, '1', '15', '0', '', '16:08:00');
INSERT INTO `venta_detalle` VALUES (412, 82, 46, '1', '20', '0', '', '16:10:00');
INSERT INTO `venta_detalle` VALUES (413, 85, 9, '0', '0', '1', '1', '16:16:00');
INSERT INTO `venta_detalle` VALUES (414, 85, 8, '1', '20', '0', '', '16:20:00');
INSERT INTO `venta_detalle` VALUES (415, 88, 30, '2', '14', '0', '', '16:27:00');
INSERT INTO `venta_detalle` VALUES (416, 88, 27, '1', '5', '0', '', '16:27:00');
INSERT INTO `venta_detalle` VALUES (417, 88, 57, '1', '10', '0', '', '16:27:00');
INSERT INTO `venta_detalle` VALUES (418, 88, 59, '1', '10', '0', '', '16:27:00');
INSERT INTO `venta_detalle` VALUES (419, 88, 39, '1', '5', '0', '', '16:36:00');
INSERT INTO `venta_detalle` VALUES (420, 89, 28, '1', '2', '0', '', '16:44:00');
INSERT INTO `venta_detalle` VALUES (421, 89, 64, '1', '2.5', '0', '', '16:47:00');
INSERT INTO `venta_detalle` VALUES (422, 90, 12, '1', '24', '0', '', '16:56:00');
INSERT INTO `venta_detalle` VALUES (423, 90, 8, '0', '0', '0', '1', '16:56:00');
INSERT INTO `venta_detalle` VALUES (424, 90, 3, '1', '24', '0', '', '16:56:00');
INSERT INTO `venta_detalle` VALUES (425, 85, 60, '0', '0', '0', '1', '17:02:00');
INSERT INTO `venta_detalle` VALUES (426, 76, 60, '0', '0', '0', '1', '17:04:00');
INSERT INTO `venta_detalle` VALUES (427, 91, 15, '1', '30', '0', '', '17:05:00');
INSERT INTO `venta_detalle` VALUES (428, 91, 4, '1', '26', '0', '', '17:05:00');
INSERT INTO `venta_detalle` VALUES (429, 91, 5, '1', '15', '0', '', '17:05:00');
INSERT INTO `venta_detalle` VALUES (430, 91, 21, '1', '35', '0', '', '17:05:00');
INSERT INTO `venta_detalle` VALUES (431, 91, 33, '1', '8', '0', '', '17:05:00');
INSERT INTO `venta_detalle` VALUES (432, 86, 60, '1', '5', '0', '', '17:06:00');
INSERT INTO `venta_detalle` VALUES (433, 90, 60, '0', '0', '0', '1', '17:07:00');
INSERT INTO `venta_detalle` VALUES (434, 90, 3, '1', '24', '0', '', '17:08:00');
INSERT INTO `venta_detalle` VALUES (435, 92, 64, '5', '12.5', '0', '', '17:10:00');
INSERT INTO `venta_detalle` VALUES (436, 91, 28, '1', '2', '0', '', '17:12:00');
INSERT INTO `venta_detalle` VALUES (437, 90, 28, '1', '2', '0', '', '17:15:00');
INSERT INTO `venta_detalle` VALUES (438, 93, 1, '1', '10', '0', '', '16:59:00');
INSERT INTO `venta_detalle` VALUES (439, 93, 2, '1', '25', '0', '', '16:59:00');
INSERT INTO `venta_detalle` VALUES (440, 93, 3, '1', '24', '0', '', '16:59:00');
INSERT INTO `venta_detalle` VALUES (441, 94, 19, '1', '38', '0', '', '16:59:00');
INSERT INTO `venta_detalle` VALUES (442, 95, 11, '0', '0', '1', '1', '17:02:00');
INSERT INTO `venta_detalle` VALUES (443, 95, 12, '1', '24', '0', '', '17:02:00');
INSERT INTO `venta_detalle` VALUES (444, 96, 1, '1', '10', '0', '', '17:04:00');
INSERT INTO `venta_detalle` VALUES (445, 96, 10, '1', '18', '0', '', '17:04:00');
INSERT INTO `venta_detalle` VALUES (446, 96, 23, '1', '12', '0', '', '17:04:00');
INSERT INTO `venta_detalle` VALUES (447, 94, 13, '2', '76', '0', '', '17:06:00');
INSERT INTO `venta_detalle` VALUES (448, 97, 1, '1', '10', '0', '', '07:32:00');
INSERT INTO `venta_detalle` VALUES (449, 98, 3, '1', '24', '1', '', '19:25:00');
INSERT INTO `venta_detalle` VALUES (450, 98, 4, '1', '26', '1', '', '19:25:00');
INSERT INTO `venta_detalle` VALUES (451, 98, 23, '1', '12', '1', '', '19:25:00');
INSERT INTO `venta_detalle` VALUES (452, 98, 27, '1', '5', '1', '', '19:25:00');
INSERT INTO `venta_detalle` VALUES (453, 99, 3, '1', '24', '1', '', '18:47:00');
INSERT INTO `venta_detalle` VALUES (454, 99, 5, '1', '15', '1', '', '18:47:00');
INSERT INTO `venta_detalle` VALUES (455, 99, 23, '1', '12', '1', '', '18:47:00');
INSERT INTO `venta_detalle` VALUES (456, 100, 11, '1', '25', '1', '', '18:47:00');
INSERT INTO `venta_detalle` VALUES (457, 100, 12, '1', '24', '1', '', '18:47:00');
INSERT INTO `venta_detalle` VALUES (458, 101, 1, '1', '11', '0', '', '10:35:00');
INSERT INTO `venta_detalle` VALUES (459, 101, 3, '1', '14', '0', '', '10:35:00');
INSERT INTO `venta_detalle` VALUES (460, 101, 5, '1', '18', '0', '', '10:35:00');
INSERT INTO `venta_detalle` VALUES (461, 102, 11, '1', '12', '1', '', '20:13:00');
INSERT INTO `venta_detalle` VALUES (462, 102, 12, '1', '14', '1', '', '20:13:00');
INSERT INTO `venta_detalle` VALUES (463, 102, 13, '1', '16', '1', '', '20:13:00');
INSERT INTO `venta_detalle` VALUES (464, 103, 3, '3', '42', '0', '', '17:26:00');
INSERT INTO `venta_detalle` VALUES (465, 104, 16, '1', '3', '0', '', '17:26:00');
INSERT INTO `venta_detalle` VALUES (466, 105, 1, '1', '11', '0', '1', '17:26:00');
INSERT INTO `venta_detalle` VALUES (467, 105, 4, '2', '32', '0', '', '17:26:00');
INSERT INTO `venta_detalle` VALUES (468, 105, 1, '1', '11', '0', '', '17:27:00');
INSERT INTO `venta_detalle` VALUES (469, 106, 3, '1', '14', '0', '', '17:33:00');
INSERT INTO `venta_detalle` VALUES (470, 107, 1, '2', '22', '1', '', '18:01:00');
INSERT INTO `venta_detalle` VALUES (471, 107, 7, '1', '15', '1', '', '18:01:00');
INSERT INTO `venta_detalle` VALUES (472, 107, 8, '1', '20', '1', '', '18:01:00');
INSERT INTO `venta_detalle` VALUES (473, 107, 1, '2', '22', '1', '', '18:03:00');
INSERT INTO `venta_detalle` VALUES (474, 108, 1, '2', '40', '1', '', '19:10:00');
INSERT INTO `venta_detalle` VALUES (475, 109, 1, '2', '40', '1', '', '19:13:00');
INSERT INTO `venta_detalle` VALUES (476, 109, 1, '2', '40', '0', '', '19:19:00');
INSERT INTO `venta_detalle` VALUES (477, 110, 12, '2', '28', '0', '', '19:00:00');
INSERT INTO `venta_detalle` VALUES (478, 111, 10, '1', '23', '0', '', '23:09:00');
INSERT INTO `venta_detalle` VALUES (479, 111, 4, '1', '23', '0', '', '23:09:00');
INSERT INTO `venta_detalle` VALUES (480, 112, 4, '1', '23', '0', '', '23:12:00');
INSERT INTO `venta_detalle` VALUES (481, 112, 10, '2', '46', '0', '', '23:12:00');
INSERT INTO `venta_detalle` VALUES (482, 113, 1, '2', '40', '0', '', '23:17:00');
INSERT INTO `venta_detalle` VALUES (483, 114, 1, '2', '40', '1', '', '23:18:00');
INSERT INTO `venta_detalle` VALUES (484, 115, 1, '1', '20', '0', '', '17:10:00');
INSERT INTO `venta_detalle` VALUES (485, 116, 1, '1', '20', '0', '', '17:13:00');
INSERT INTO `venta_detalle` VALUES (486, 116, 2, '1', '20', '0', '', '17:13:00');
INSERT INTO `venta_detalle` VALUES (487, 117, 1, '1', '20', '0', '', '17:18:00');
INSERT INTO `venta_detalle` VALUES (488, 117, 2, '1', '20', '0', '', '17:18:00');
INSERT INTO `venta_detalle` VALUES (489, 117, 15, '1', '3', '0', '', '17:18:00');
INSERT INTO `venta_detalle` VALUES (490, 118, 1, '1', '20', '0', '', '17:21:00');
INSERT INTO `venta_detalle` VALUES (491, 118, 2, '1', '20', '0', '', '17:21:00');
INSERT INTO `venta_detalle` VALUES (492, 119, 1, '1', '20', '1', '', '17:35:00');
INSERT INTO `venta_detalle` VALUES (493, 119, 2, '1', '20', '0', '', '17:35:00');
INSERT INTO `venta_detalle` VALUES (494, 120, 2, '1', '20', '0', '', '17:35:00');
INSERT INTO `venta_detalle` VALUES (495, 120, 1, '3', '60', '0', '', '17:35:00');
INSERT INTO `venta_detalle` VALUES (496, 121, 1, '2', '40', '1', '', '17:35:00');
INSERT INTO `venta_detalle` VALUES (497, 121, 2, '3', '60', '0', '', '17:35:00');
INSERT INTO `venta_detalle` VALUES (498, 122, 1, '1', '20', '1', '', '00:53:00');
INSERT INTO `venta_detalle` VALUES (499, 122, 2, '1', '20', '1', '', '00:53:00');
INSERT INTO `venta_detalle` VALUES (500, 122, 3, '1', '20', '1', '', '00:53:00');
INSERT INTO `venta_detalle` VALUES (501, 122, 9, '1', '23', '1', '', '00:56:00');
INSERT INTO `venta_detalle` VALUES (502, 123, 1, '1', '20', '0', '', '01:03:00');
INSERT INTO `venta_detalle` VALUES (503, 123, 2, '1', '20', '0', '', '01:03:00');
INSERT INTO `venta_detalle` VALUES (504, 124, 1, '1', '20', '0', '', '01:50:00');
INSERT INTO `venta_detalle` VALUES (505, 124, 2, '0', '0', '1', '1', '01:50:00');
INSERT INTO `venta_detalle` VALUES (506, 124, 3, '0', '0', '1', '1', '01:50:00');
INSERT INTO `venta_detalle` VALUES (507, 125, 17, '2', '6', '1', '2', '19:55:00');
INSERT INTO `venta_detalle` VALUES (508, 125, 16, '4', '12', '0', '', '19:55:00');
INSERT INTO `venta_detalle` VALUES (509, 126, 11, '3', '36', '0', '1', '20:11:00');
INSERT INTO `venta_detalle` VALUES (510, 127, 1, '0', '0', '1', '1', '02:31:00');
INSERT INTO `venta_detalle` VALUES (511, 127, 2, '1', '20', '0', '', '02:31:00');
INSERT INTO `venta_detalle` VALUES (512, 128, 1, '1', '20', '0', '', '21:58:00');
INSERT INTO `venta_detalle` VALUES (513, 129, 20, '1', '20', '0', '', '11:36:00');
INSERT INTO `venta_detalle` VALUES (514, 130, 3, '1', '20', '1', '', '16:02:00');
