ALTER TABLE mesa ADD COLUMN fk_persona INT NULL;

CREATE TABLE out_of_stock (
    id_out_of_stock INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    fk_persona INT NOT NULL,
    fk_producto INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp() 
);

SELECT
    oos.id_out_of_stock,
    oos.created_at,
    p.pNombres AS "nombre_persona",
    p.pApellidos AS "apellido_persona",
    p.pAlias AS "alias_persona",
    pr.pNombre AS "nombre_producto",
    pr.pDescripcion AS "descripcion_producto"
FROM
out_of_stock oos
LEFT JOIN
    persona p ON
        p.pId = oos.fk_persona
LEFT JOIN
    producto pr ON
        pr.idProducto = oos.fk_producto
ORDER BY
    oos.created_at DESC;
